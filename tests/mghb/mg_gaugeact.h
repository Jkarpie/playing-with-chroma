// -*- C++ -*-
/*! \file
 *  \brief Plaquette gauge action
 */

#ifndef __mg_gaugeact_h__
#define __mg_gaugeact_h__

#include "gaugeact.h"
#include "gaugebc.h"
#include "actions/gauge/gaugestates/gauge_createstate_factory.h"
#include "actions/gauge/gaugestates/gauge_createstate_aggregate.h"
#include "io/aniso_io.h"

namespace Chroma
{

  /*! @ingroup gaugeacts */
  namespace MgGaugeActEnv 
  { 
    extern const std::string name;
    bool registerAll();
  }
  
//  /*! @ingroup gaugeacts */
//  void write(XMLWriter& xml, const std::string& path, const MgGaugeActParams& param);
  

  //! MultiGrid gauge action
  /*! \ingroup gaugeacts
   *
   * The standard Plaquette gauge action
   */


  // QUESTION: Would this be better to put into the h file?
  // A coloring function to find the active sites on the coarse lattice (when color = 0).
  class ColorFunc : public SetFunc{
  private:
    int l ;
  public:
    ColorFunc(int l_):l(l_)  {}
  
    int operator()(const multi1d<int>& coord) const {
      int a=0;
      for(int mu=0; mu < Nd; ++mu){a+=coord[mu]%(1<<l);}
      return a;
    }
    int numSubsets() const {
      return 4*((1<<l)-1)+1;// ex: for l=1, the possible values are 0, 1, 2, 3, 4
    }  
  } ;


/*// QUESTION: Kostas teach me how to use maps, so shifter can use one jump instead of 2^l
// This function is for shifter Map, which shifts multiple steps
struct shifterFunc : public MapFunc{
private:
  int dir;
  int dist;
  int sign;
public:
  shifter(int dir_, int sign_, int dist_): dir(dir_), sign(sign_), dist(dist_) {}
  multi1d<int> operator()(const multi1d<int>& x) const{
    multi1d<int> newx(Nd);
    for(int i=0;i<Nd;++i) newx[i]=x[i];
    // QUESTION: Below needs check for when shifting around the boundary conditions. How to ask for the Lattice Size in a given direction?
    multi1d<int>size;
    bounds=Layout::getLattSize();
    if(sign==FORWARD){newx[dir]+=dist % size[dir];}
    if(sign==BACKWARD){
      newx[dir]-=dist; 
      if(newx[dir]<0) newx[dir]+=size[dir];
    }
    return newx;
  }
};*/


  //! Parameter structure
  /*! @ingroup gaugeacts */
  class MgGaugeActParams 
  {
  public:
    //! Base Constructor
    MgGaugeActParams() {}
    ~MgGaugeActParams() {}
  
    //! Read params from some root path
    MgGaugeActParams(XMLReader& xml_in, const std::string& path); 

    Real  beta;
    int l;
    multi2d<LatticeColorMatrix> a;
    multi2d<LatticeColorMatrix> b;
    multi2d<LatticeColorMatrix> c;
    multi2d<LatticeColorMatrix> d;
    multi1d<LatticeColorMatrix> h;
  };

  class MgGaugeAct : public LinearGaugeAction
  {
  public:

    // A constructor to initialize without using a registered string name
    MgGaugeAct(XMLReader& xml, const std::string& path) 
    { 
      MgGaugeAct(CreateGaugeStateEnv::reader(xml,path),MgGaugeActParams(xml,path));
      /*Handle< CreateGaugeState<P,Q> > cgs_;
      cgs_ = CreateGaugeStateEnv::reader(xml,path);
      MgGaugeActParams p = MgGaugeActParams(xml,path);
      cgs = cgs_;
      param = p;
      //QUESTION: Do I need to assign multi#d<LCM> with for loops like you would with pointers? Or does the prettier thing below work?
      a.resize(Nd,Nd);
      b.resize(Nd,Nd);
      c.resize(Nd,Nd);
      d.resize(Nd,Nd);
      h.resize(Nd);
      a=param.a;
      b=param.b;
      c=param.c;
      d=param.d;
      h=param.h;
//        shifter.make(shifterFunc());
      color.make(ColorFunc(param.l));*/
    }

    //! Read coeffs from a param struct
    MgGaugeAct(Handle< CreateGaugeState<P,Q> > cgs_, 
		 const MgGaugeActParams& p) :
      cgs(cgs_), param(p) {

        a.resize(Nd,Nd);
        b.resize(Nd,Nd);
        c.resize(Nd,Nd);
        d.resize(Nd,Nd);
        h.resize(Nd);
        a=param.a;
        b=param.b;
        c=param.c;
        d=param.d;
        h=param.h;
//        shifter.make(shifterFunc());
        color.make(ColorFunc(param.l));
    }

    //! Return the set on which the gauge action is defined
    /*! Defined on the even-off (red/black) set */
    const Set& getSet() const {return color;}

    //! Compute staple
    void staple(LatticeColorMatrix& result,
		const Handle< GaugeState<P,Q> >& state,
		int mu, int cb) const;

    //! Compute dS/dU
    void deriv(multi1d<LatticeColorMatrix>& result,
	       const Handle< GaugeState<P,Q> >& state) const;

    //! Produce a gauge create state object
    const CreateGaugeState<P,Q>& getCreateState() const {return *cgs;}


    //! Compute the actions
    Double S(const Handle< GaugeState<P,Q> >& state) const;


    //! Destructor is automatic
    ~MgGaugeAct() {}

  protected:
    MgGaugeAct() {}
    void operator=(const MgGaugeAct& a) {}       //! Hide assignment


  private:
    Handle< CreateGaugeState<P,Q> >  cgs;  /*!< Create Gauge State */
    MgGaugeActParams  param;             /*!< The parameters */
    Set color;

    //! Shifts LCM
    LatticeColorMatrix shifter(const LatticeColorMatrix& a, int sign, int dir, int dist);

    multi2d<LatticeColorMatrix> a;
    multi2d<LatticeColorMatrix> b;
    multi2d<LatticeColorMatrix> c;
    multi2d<LatticeColorMatrix> d;
    multi1d<LatticeColorMatrix> h;
    //Map shifter;
  };

};


#endif
