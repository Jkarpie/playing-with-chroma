/*! \file
 *  \brief Plaquette gauge action
 */

#include "chromabase.h"
#include "actions/gauge/gaugeacts/mg_gaugeact.h"
#include "mg_gaugeact.h"
#include "actions/gauge/gaugeacts/gaugeact_factory.h"
#include "actions/gauge/gaugestates/gauge_createstate_factory.h"
#include "actions/gauge/gaugestates/gauge_createstate_aggregate.h"
#include "meas/glue/mesplq.h"


namespace Chroma
{
 
  namespace MgGaugeActEnv 
  { 
    GaugeAction< multi1d<LatticeColorMatrix>, 
		 multi1d<LatticeColorMatrix> >* createGaugeAct(XMLReader& xml, 
							       const std::string& path) 
    {
      return new MgGaugeAct(CreateGaugeStateEnv::reader(xml, path), 
			      MgGaugeActParams(xml, path));
    }

    const std::string name = "MG_GAUGEACT";

    //! Local registration flag
    static bool registered = false;

    //! Register all the factories
    bool registerAll() 
    {
      bool success = true; 
      if (! registered)
      {
	success &= TheGaugeActFactory::Instance().registerObject(name, createGaugeAct);
	registered = true;
      }
      return success;
    }
  }

  // Params given beta, a,b,c,d,h, and l
  MgGaugeActParams::MgGaugeActParams(XMLReader& xml_in, const std::string& path) 
  {
    try{ 
      XMLReader paramtop(xml_in, path);
      h=paramtop.h;
      l=paramtop.l;
      beta=paramtop.beta;
      a.resize(Nd,Nd);
      b.resize(Nd,Nd);
      c.resize(Nd,Nd);
      d.resize(Nd,Nd);
      h.resize(Nd);
      for(int m=0; m<Nd; ++m){
        XMLReader reader(paramtop,std::to_string(m));
        a[m]=reader.a;
        b[m]=reader.b;
        c[m]=reader.c;
        d[m]=reader.d;
      }
    }
    catch( const std::string& e ) { 
      QDPIO::cerr << "Error reading XML: " <<  e << std::endl;
      QDP_abort(1);
    }
  }


  void read(XMLReader& xml, const std::string& path, MgGaugeActParams& p) 
  {
    MgGaugeActParams tmp(xml, path);
    p=tmp;
  }


// Returns LCM a shifted by dist steps
// The use of this method as written is inefficient for higher l, in most cases dist = 2^l
  MgGaugeAct::LatticeColorMatrix shifter(const LatticeColorMatrix& a, int sign, int dir, int dist){
    LatticeColorMatrix b;
    LatticeColorMatrix c;
    b=a;
    for(int i=0;i<dist;++i){
      c=shift(b,sign,dir);
      b=c;
    }
    return b;
  }

  //! Compute staple
  /*!
   * \param u_mu_staple   result      ( Write )
   * \param state         gauge field ( Read )
   * \param mu            direction for staple ( Read )
   * \param cb            subset on which to compute ( Read )
   */
  void
  MgGaugeAct::staple(LatticeColorMatrix& u_mu_staple,
		       const Handle< GaugeState<P,Q> >& state,
		       int mu, int cb) const
  {
    START_CODE();

    const multi1d<LatticeColorMatrix>& u = state->getLinks();
    
    u_mu_staple = zero;
    LatticeColorMatrix tmp1, tmp2;
    LatticeColorMatrix u_nu_mu;
    if(param.l==0){
      // This is the original plaq_gaugeact code
      for(int nu=0; nu < Nd; ++nu) 
      {
        if( nu == mu ) continue;
      
        u_nu_mu = shift(u[nu],FORWARD,mu);

        // +forward staple
        tmp1[rb[cb]] = u_nu_mu * adj(shift(u[mu],FORWARD,nu));
        tmp2[rb[cb]] = tmp1 * adj(u[nu]);

        u_mu_staple[rb[cb]] += param.coeffs[mu][nu] * tmp2;

        // +backward staple
        tmp1[rb[cb]] = adj(shift(u_nu_mu,BACKWARD,nu)) * adj(shift(u[mu],BACKWARD,nu));
        tmp2[rb[cb]] = tmp1 * shift(u[nu],BACKWARD,nu);

        u_mu_staple[rb[cb]] += tmp2 * Double(param.beta);
      }
    }
    else if(param.l>0){
      LatticeColorMatrix temp;
      temp=0;
      for(int nu=0; nu < Nd; ++nu) 
      {
        if( nu == mu ) continue;
        temp[color[0]]=param.a[mu][nu]*adj(u[nu]) + 
             shifter(u[nu],FORWARD,mu,1<<param.l)*param.b[mu][nu] + 
             param.c[mu][nu]*shifter(u[nu],BACKWARD,nu,1<<param.l) + 
             shifter(shifter(adj(u[nu]),BACKWARD,nu,1<<param.l),FORWARD,mu,1<<param.l)*param.d[mu][nu];
      }
      u_mu_staple[color[0]]=(temp+param.h[mu]) * Double(param.beta);
    }

    // NOTE: a heatbath code should be responsible for resetting links on
    // a boundary. The staple is not really the correct place.

    END_CODE();
  }

  Double
  MgGaugeAct::S(const Handle< GaugeState<P,Q> >& state) const
  {
    START_CODE();

    Double S_pg = zero;

    // Handle< const GaugeState<P,Q> > u_bc(createState(u));
    // Apply boundaries
    const multi1d<LatticeColorMatrix>& u = state->getLinks();

    // Compute the average plaquettes
    if(param.l==0){
      // This is the original plaq_gaugeact code
      for(int mu=1; mu < Nd; ++mu)
      {
        for(int nu=0; nu < mu; ++nu)
        {
	  Double tmp = 
		sum(real(trace(u[mu]*shift(u[nu],FORWARD,mu)*adj(shift(u[mu],FORWARD,nu))*adj(u[nu]))));

          S_pg += tmp * Double(param.beta);
        }
      }
    }

    else if(param.l>0){
      for(int mu=0; mu<Nd; ++mu)
      {
        LatticeColorMatrix temp;
        Double tmp=zero;
        temp=0;
        for(int nu=0; nu < mu; ++nu)
        {
          temp[color[0]]=(param.a[mu][nu]*adj(u[nu]) + 
		shifter(u[nu],FORWARD,mu,1<<param.l)*param.b[mu][nu] + 
		param.c[mu][nu]*shifter(u[nu],BACKWARD,nu,1<<param.l) + 
		shifter(shifter(adj(u[nu]),BACKWARD,nu,1<<param.l),FORWARD,mu,1<<param.l)*param.d[mu][nu])
		*u[mu];
          tmp=sum(real(trace(temp)));
          S_pg += tmp;
        }
        temp[color[0]] = param.h[mu]*u[mu];// This term will be 0 for l=1 case
        tmp=sum(real(trace(temp)));
        S_pg += tmp * Double(param.beta);
      }
    }

    // Normalize
    S_pg *= Double(-1.0)/Double(Nc);
    
    END_CODE();

    return S_pg;
  } 


  //! Computes the derivative of the fermionic action respect to the link field
  /*!
   *         |  dS
   * ds_u -- | ----   ( Write )
   *         |  dU  
   *
   * \param ds_u       result      ( Write )
   * \param state      gauge field ( Read )
   */
  void
  MgGaugeAct::deriv(multi1d<LatticeColorMatrix>& ds_u,
		      const Handle< GaugeState<P,Q> >& state) const
  {
    START_CODE();

    ds_u.resize(Nd);
    ds_u = zero;
    const multi1d<LatticeColorMatrix>& u = state->getLinks();

    if(param.l==0){
      // This is the original plaq_gaugeact code
      LatticeColorMatrix tmp_0;
      LatticeColorMatrix tmp_1;
      LatticeColorMatrix tmp_2;

      for(int mu = 0; mu < Nd; mu++)
      {
        for(int nu=mu+1; nu < Nd; nu++) 
        {
	  for(int cb=0; cb < 2; cb++) 
          { 
            tmp_0[rb[cb]] = shift(u[mu], FORWARD, nu)*shift(adj(u[nu]), FORWARD, mu);
            tmp_0[rb[cb]] *= param.beta;   // c[mu][nu] = c[nu][mu]
            tmp_1[rb[cb]] = tmp_0*adj(u[mu]);
            tmp_2[rb[cb]] = u[nu]*tmp_1;
            ds_u[nu][rb[cb]] += tmp_2;
            ds_u[mu][rb[cb]] += adj(tmp_2);
            ds_u[mu][rb[1-cb]] += shift(tmp_1, BACKWARD, nu)*shift(u[nu], BACKWARD, nu);
            tmp_1[rb[cb]] = adj(u[nu])*u[mu];
            ds_u[nu][rb[1-cb]] += shift(adj(tmp_0),BACKWARD,mu)*shift(tmp_1, BACKWARD, mu);
	  }
        }
      
        // It is 1/(4Nc) to account for normalisation relevant to fermions
        // in the taproj, which is a factor of 2 different from the 
        // one used here.
        ds_u[mu] *= Real(-1)/(Real(2*Nc));
      }
    }
    else if(param.l>0){
      for(int mu=0; mu<Nd; ++mu){
        // The below process will double count the corner terms, so the initial plaq_gaugeact code is used to undo this problem
        LatticeColorMatrix s;
        staple(s,state,mu,0)
        ds_u[mu][color[0]]+= u[mu]*s +adj(u[mu]*s);

        // To fix double counting
        for(int nu=mu+1; nu < Nd; nu++) 
        {
          LatticeColorMatrix plaq;
          plaq=u[mu]*shift(u[nu],FORWARD,mu)*adj(shift(u[mu],FORWARD,nu))*adj(u[nu]);
          ds_u[mu][color[0]]-=plaq + shift(plaq,BACKWARD,mu) + shift(plaq,BACKWARD,nu) + shift(shift(plaq,BACKWARD,mu),BACKWARD,nu);
        }
        ds_u[mu][color[0]] *= Real(-1)/(Real(2*Nc));
      }
    }

    // Zero the force on any fixed boundaries
    getGaugeBC().zero(ds_u);

    END_CODE();
  }
}

