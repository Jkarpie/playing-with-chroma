/*! \file
 *  \brief Main code for pure gauge field generation
 */

#include "chroma.h"
#include "actions/gauge/gaugeacts/gaugeacts_aggregate.h"
#include "mg_gaugeact.h"

using namespace Chroma;


// A coloring function to find the active sites on the coarse lattice (when color = 0).
/*class ColorFunc : public SetFunc{
private:
  int l ;
public:
  ColorFunc(int l_):l(l_)  {}
  
  int operator()(const multi1d<int>& coord) const {
    int a=0;
    for(int mu=0; mu < Nd; ++mu){a+=coord[mu]%(1<<l);}
    return a;
  }
  int numSubsets() const {
    return 4*((1<<l)-1)+1;// ex: for l=1, the possible values are 0, 1, 2, 3, 4
  }  
} ;*/

/*// This code needs a safety for if it runs beyond the bounds of the lattice. How do you prompt for lattice dimensions or another way to do this?
// This function is for shifter Map, which shifts multiple steps
struct shifterFunc : public MapFunc{
private:
  int dir;
  int dist;
  int sign;
public:
  // What is the syntax for more than one variable? Do I need more :  below or something?
  shifterFunct(int dir_, int sign_, int dist_): dir(dir_) sign(sign_) dist(dist_) {}
  multi1d<int> operator()(const multi1d<int>& coord) const{
    multi1d<int> newcoord(Nd);
    for(int i=0;i<Nd;++i) newcoord[i]=coord[i];
    // Below requires a safety net for when shifting around the boundary conditions. How to ask for the Lattice Size in a given direction?
    if(sign==FORWARD){newcoord[dir]+=dist;}
    if(sign==BACKWARD){newcoord[dir]-=dist;}
    return newcoord;
  }
};*/

namespace Chroma 
{

  //! Holds gauge action
  struct HBGauge
  {
    std::string  gauge_act;     /*!<  Holds gauge action xml */
  };


  //! Read the parameters
  void read(XMLReader& xml_in, const std::string& path, HBGauge& p)
  {
    try {
      // Read the inverter Parameters
      XMLReader xml_tmp(xml_in, "./GaugeAction");
      std::ostringstream os;
      xml_tmp.print(os);
      p.gauge_act = os.str();
    }
    catch(const std::string& s) {
      QDPIO::cerr << "Caught Exception while reading gauge action: " << s <<std::endl;
      QDP_abort(1);
    }

    QDPIO::cout << "Gauge action: read \n" << p.gauge_act << std::endl;
  }


  //! Writer
  void write(XMLWriter& xml, const std::string& path, const HBGauge& p)
  {
    xml << p.gauge_act;
  }


  //! Reader
  void read(XMLReader& xml, const std::string& path, HBParams& p)
  {
    try { 
      XMLReader paramtop(xml, path);
      read(paramtop, "NmaxHB", p.NmaxHB);
      read(paramtop, "nOver", p.nOver);
    }
    catch(const std::string& e ) { 
      QDPIO::cerr << "Caught Exception reading HBParams: " << e << std::endl;
      QDP_abort(1);
    }
  }

  //! Writer
  void write(XMLWriter& xml, const std::string& path, const HBParams& p)
  {
    push(xml, path);

    write(xml, "NmaxHB", p.NmaxHB);
    write(xml, "nOver", p.nOver);

    pop(xml);
  }


  //! Params controlling running of monte carlo
  struct MCControl 
  {
    QDP::Seed rng_seed;
    unsigned long start_update_num;
    unsigned long n_warm_up_updates;
    unsigned long n_production_updates;
    unsigned int  n_updates_this_run;
    unsigned int  save_interval;
    std::string   save_prefix;
    QDP_volfmt_t  save_volfmt;
  };

  void read(XMLReader& xml, const std::string& path, MCControl& p) 
  {
    try { 
      XMLReader paramtop(xml, path);
      read(paramtop, "./RNG", p.rng_seed);
      read(paramtop, "./StartUpdateNum", p.start_update_num);
      read(paramtop, "./NWarmUpUpdates", p.n_warm_up_updates);
      read(paramtop, "./NProductionUpdates", p.n_production_updates);
      read(paramtop, "./NUpdatesThisRun", p.n_updates_this_run);
      read(paramtop, "./SaveInterval", p.save_interval);
      read(paramtop, "./SavePrefix", p.save_prefix);
      read(paramtop, "./SaveVolfmt", p.save_volfmt);

      if (p.n_updates_this_run % p.save_interval != 0)
	throw std::string("UpdateThisRun not a multiple of SaveInterval");
    }
    catch(const std::string& e ) { 
      QDPIO::cerr << "Caught Exception reading MCControl: " << e << std::endl;
      QDP_abort(1);
    }
  }

  void write(XMLWriter& xml, const std::string& path, const MCControl& p) 
  {
    push(xml, path);

    write(xml, "RNG", p.rng_seed);
    write(xml, "StartUpdateNum", p.start_update_num);
    write(xml, "NWarmUpUpdates", p.n_warm_up_updates);
    write(xml, "NProductionUpdates", p.n_production_updates);
    write(xml, "NUpdatesThisRun", p.n_updates_this_run);
    write(xml, "SaveInterval", p.save_interval);
    write(xml, "SavePrefix", p.save_prefix);
    write(xml, "SaveVolfmt", p.save_volfmt);

    pop(xml);
  }


  //! Holds params for Heat-bath
  struct HBItrParams 
  { 
    multi1d<int> nrow;

    HBGauge   hb_gaugeact;    /*!< This is polymorphic */
    HBParams  hb_params;      /*!< Solely the HB bit */
  };

  void write(XMLWriter& xml, const std::string& path, const HBItrParams& p)
  {
    push(xml, path);
    write(xml, "nrow", p.nrow);
    write(xml, "GaugeAction", p.hb_gaugeact);
    write(xml, "HBParams", p.hb_params);
    pop(xml);
  }


  void read(XMLReader& xml, const std::string& path, HBItrParams& p) 
  {
    try {
      XMLReader paramtop(xml, path);
      
      read(paramtop, "nrow", p.nrow);
      read(paramtop, "GaugeAction", p.hb_gaugeact);
      read(paramtop, "HBParams", p.hb_params);
    }
    catch( const std::string& e ) { 
      QDPIO::cerr << "Error reading HBItrParams XML : " << e << std::endl;
      QDP_abort(1);
    }
  }

  //! Main struct from input params and output restarts
  struct MGControl 
  {
    HBItrParams   hbitr_params;
    MCControl     mc_control;
    Cfg_t         cfg;
    std::string   inline_measurement_xml;

    // New params for multigrid control
    unsigned int  npre;
    unsigned int  npost;
    unsigned int  gam;
    unsigned int lmax;
  };


  //! Reader
  void read(XMLReader& xml_in, const std::string& path, MGControl& p) 
  {
    try {
      XMLReader paramtop(xml_in, path);

      read(paramtop, "HBItr", p.hbitr_params);
      read(paramtop, "MCControl", p.mc_control);
      read(paramtop, "Cfg", p.cfg);

      read(paramtop, "npre", p.npre);
      read(paramtop, "npost", p.npost);
      read(paramtop, "gam", p.gam);
      read(paramtop, "lmax", p.lmax);

      if( paramtop.count("./InlineMeasurements") == 0 ) {
	XMLBufferWriter dummy;
	push(dummy, "InlineMeasurements");
	pop(dummy); // InlineMeasurements
	p.inline_measurement_xml = dummy.printCurrentContext();
      }
      else 
      {
	XMLReader measurements_xml(paramtop, "./InlineMeasurements");
	std::ostringstream inline_os;
	measurements_xml.print(inline_os);
	p.inline_measurement_xml = inline_os.str();
	QDPIO::cout << "InlineMeasurements are: " << std::endl;
	QDPIO::cout << p.inline_measurement_xml << std::endl;
      }
    }
    catch(const std::string& e) {
      QDPIO::cerr << "Caught Exception reading MGControl: " << e << std::endl;
      QDP_abort(1);
    }
  }


  //! Writer
  void write(XMLWriter& xml, const std::string& path, const MGControl& p) 
  {
    push(xml, path);

    write(xml, "Cfg", p.cfg);
    write(xml, "MCControl", p.mc_control);
    xml << p.inline_measurement_xml;
    write(xml, "HBItr", p.hbitr_params);

    write(xml, "npre", p.npre);
    write(xml, "npost", p.npost);
    write(xml, "gam", p.gam);
    write(xml, "lmax", p.lmax);

    pop(xml);
  }



  //--------------------------------------------------------------------------
  // Specialise
  MCControl newMCHeader(const HBItrParams& update_params, 
			const MCControl& mc_control,
			unsigned long update_no)
  {
    START_CODE();

    // Copy old params
    MCControl p_new = mc_control;
    
    // Get Current RNG Seed
    QDP::RNG::savern(p_new.rng_seed);
   
    // Set the current traj number
    p_new.start_update_num = update_no;
    
    // Reset the warmups
    p_new.n_warm_up_updates = 0;
    
    // Set the num_updates_this_run
    unsigned long total = mc_control.n_production_updates;

    if ( total < mc_control.n_updates_this_run + update_no ) { 
      p_new.n_updates_this_run = total - update_no;
    }

    END_CODE();

    return p_new;
  }



  // Specialise
  void saveState(const HBItrParams& update_params, 
		 MCControl& mc_control,
		 unsigned long update_no,
		 const std::string& inline_measurement_xml,
		 const multi1d<LatticeColorMatrix>& u)
  {
    START_CODE();

    MCControl mc_new = newMCHeader(update_params, mc_control, update_no);

    // Files
    std::ostringstream restart_data_filename;
    std::ostringstream restart_config_filename;

    unsigned long save_num = update_no / mc_control.save_interval;
    restart_data_filename << mc_control.save_prefix << ".ini.xml" << save_num;
    restart_config_filename << mc_control.save_prefix << ".lime" << save_num;
    
    {
      MGControl hb;
      hb.hbitr_params = update_params;
      hb.mc_control = mc_new;
      hb.inline_measurement_xml = inline_measurement_xml;

      // Set the name of the restart file
      hb.cfg.cfg_file = restart_config_filename.str();

      // Hijack this for now and assumes it means what I want it to mean
      hb.cfg.cfg_type = CFG_TYPE_SZINQIO;

      // Write a restart DATA file from the buffer XML
      XMLFileWriter restart_xml(restart_data_filename.str().c_str());
      write(restart_xml, "purgaug", hb);
      restart_xml.close();
    }

    {
      // Save the config

      // some dummy header for the file
      XMLBufferWriter file_xml;
      push(file_xml, "HB");
      proginfo(file_xml);
      pop(file_xml);

      XMLBufferWriter config_xml;
      push(config_xml, "ChromaHB");
      write(config_xml, "MCControl", mc_new);
      write(config_xml, "HBItr", update_params);
      pop(config_xml);

      // Save the config
      writeGauge(file_xml, 
		 config_xml,
		 u,
		 restart_config_filename.str(),
		 mc_new.save_volfmt,
		 QDPIO_SERIAL);    
    }
    
    END_CODE();
  }


  //--------------------------------------------------------------------------
  void doMeas(XMLWriter& xml_out,
	      multi1d<LatticeColorMatrix>& u,
	      MGControl& mg_control, 
	      bool warm_up_p,
	      unsigned long cur_update,
	      const multi1d< Handle< AbsInlineMeasurement > >& default_measurements,
	      const multi1d< Handle<AbsInlineMeasurement> >& user_measurements) 
  {
    START_CODE();

    // Create a gauge header for inline measurements.
    // Since there are defaults always measured, we must always
    // create a header.
    //
    // NOTE: THIS HEADER STUFF NEEDS A LOT MORE THOUGHT
    //
    MCControl mc_new = newMCHeader(mg_control.hbitr_params, mg_control.mc_control, cur_update);

    XMLBufferWriter gauge_xml;
    push(gauge_xml, "ChromaHB");
    write(gauge_xml, "MCControl", mc_new);
    write(gauge_xml, "HBItr", mg_control.hbitr_params);
    pop(gauge_xml);

    // Reset and set the default gauge field
    InlineDefaultGaugeField::reset();
    InlineDefaultGaugeField::set(u, gauge_xml);

    // Measure inline observables 
    push(xml_out, "InlineObservables");

    // Always measure defaults
    for(int m=0; m < default_measurements.size(); m++) 
    {
      // Caller writes elem rule 
      AbsInlineMeasurement& the_meas = *(default_measurements[m]);
      push(xml_out, "elem");
      the_meas(cur_update, xml_out);
      pop(xml_out);
    }
	
    // Only measure user measurements after warm up
    if( ! warm_up_p ) 
    {
      QDPIO::cout << "Doing " << user_measurements.size() 
		  <<" user measurements" << std::endl;
      for(int m=0; m < user_measurements.size(); m++) 
      {
	AbsInlineMeasurement& the_meas = *(user_measurements[m]);
	if( cur_update % the_meas.getFrequency() == 0 ) 
	{ 
	  // Caller writes elem rule
	  push(xml_out, "elem");
	  the_meas(cur_update, xml_out );
	  pop(xml_out); 
	}
      }
    }
    pop(xml_out); // pop("InlineObservables");

    // Reset the default gauge field
    InlineDefaultGaugeField::reset();
    
    END_CODE();
  }

  // I got it to ok calling mgmciter if I pasted it here in the code itself as opposed to finding the mgmciter.h file and pulling from there. I do not understand why that doesn't work.
  void mgmciter(multi1d<LatticeColorMatrix>& u, 
	      const LinearGaugeAction& S_g,
	      const HBParams& hbp,
              Set color)
  {
    START_CODE();

    LatticeColorMatrix u_mu_staple;
    int ntrials = 0;
    int nfails = 0;

    const Set& gauge_set = S_g.getSet();
    const int num_subsets = gauge_set.numSubsets();

    for(int iter = 0; iter <= hbp.nOver; ++iter)
    {
      for(int cb = 0; cb < 1; ++cb)
      {
	for(int mu = 0; mu < Nd; ++mu)
	{
	  // Calculate the staple
	  {
	    typedef multi1d<LatticeColorMatrix>  P;
	    typedef multi1d<LatticeColorMatrix>  Q;

	    Handle< GaugeState<P,Q> > state(S_g.createState(u));

	    //staple 
	    S_g.staple(u_mu_staple, state, mu, cb);
	  }

	  if ( iter < hbp.nOver )
	  {
	    /* Do an overrelaxation step */
	    /*# Loop over SU(2) subgroup index */
	    for(int su2_index = 0; su2_index < Nc*(Nc-1)/2; ++su2_index)
	      su3over(u[mu], u_mu_staple, su2_index, gauge_set[cb]);
	  }
	  else
	  {
	    /* Do a heatbath step */
	    /*# Loop over SU(2) subgroup index */
	    for(int su2_index = 0; su2_index < Nc*(Nc-1)/2; ++su2_index)
	    {
	      int ntry = 0;
	      int nfail = 0;

	      su2_hb_update(u[mu], u_mu_staple,
			    Real(2.0/Nc),
			    su2_index, gauge_set[cb],
			    hbp.nmax());

//	      su3hb(u[mu], u_mu_staple, su2_index, nheat, ntry, nfail, gauge_set[cb]);
	      ntrials += ntry;
	      nfails += nfail;
	    }
          
	    /* Reunitarize */
	    reunit(u[mu]);

	  }

	  // If using Schroedinger functional, reset the boundaries
	  // NOTE: this routine resets all links and not just those under mu,cb
	  S_g.getGaugeBC().modify(u);

	}    // closes mu loop
      }      // closes cb loop
    }

  
    END_CODE();
  }
  


  //--------------------------------------------------------------------------
  void doWarmUp(XMLWriter& xml_out,
		multi1d<LatticeColorMatrix>& u,
		const LinearGaugeAction& S_g,
		MGControl& mg_control,
		const multi1d< Handle< AbsInlineMeasurement > >& default_measurements,
		const multi1d< Handle<AbsInlineMeasurement> >& user_measurements,
                Set color) 
  {
    START_CODE();

    // Set the update number
    unsigned long cur_update = 0;
      
    // Compute how many updates to do
    unsigned long to_do = mg_control.mc_control.n_warm_up_updates;
      
    QDPIO::cout << "WarmUp Control: About to do " << to_do << " updates" << std::endl;

    // XML Output
    push(xml_out, "WarmUpdates");

    for(int i=0; i < to_do; i++) 
    {
      push(xml_out, "elem"); // Caller writes elem rule

      push(xml_out, "Update");
      // Increase current update counter
      cur_update++;
	
      // Log
      write(xml_out, "update_no", cur_update);
      write(xml_out, "WarmUpP", true);

      // QUESTION: How do I tell it to find this new method in (mg)mciter instead of using one I placed above?
      // Do the update, but with no measurements
      mgmciter(u, S_g, mg_control.hbitr_params.hb_params,color); //one hb sweep

      // Do measurements
      doMeas(xml_out, u, mg_control, true, cur_update,
	     default_measurements, user_measurements);

      pop(xml_out); // pop("Update");
      pop(xml_out); // pop("elem");
    }

    pop(xml_out); // pop("WarmUpdates")
    
    END_CODE();
  }
  

  //--------------------------------------------------------------------------
  void doProd(XMLWriter& xml_out,
	      multi1d<LatticeColorMatrix>& u,
	      const LinearGaugeAction& S_g,
	      MGControl& mg_control, 
	      const multi1d< Handle< AbsInlineMeasurement > >& default_measurements,
	      const multi1d< Handle<AbsInlineMeasurement> >& user_measurements,
                Set color) 
  {
    START_CODE();

    // Set the update number
    unsigned long cur_update = mg_control.mc_control.start_update_num;
      
    // Compute how many updates to do
    unsigned long total_updates = mg_control.mc_control.n_production_updates;
      
    unsigned long to_do = 0;
    if ( total_updates > mg_control.mc_control.n_updates_this_run + cur_update +1 ) {
      to_do = mg_control.mc_control.n_updates_this_run;
    }
    else {
      to_do = total_updates - cur_update ;
    }
      
    QDPIO::cout << "MC Control: About to do " << to_do << " updates" << std::endl;

    // XML Output
    push(xml_out, "MCUpdates");

    for(int i=0; i < to_do; i++) 
    {
      push(xml_out, "elem"); // Caller writes elem rule

      push(xml_out, "Update");
      // Increase current update counter
      cur_update++;
	
      // Decide if the next update is a warm up or not
      QDPIO::cout << "Doing Update: " << cur_update << " warm_up_p = " << false << std::endl;

      // Log
      write(xml_out, "update_no", cur_update);
      write(xml_out, "WarmUpP", false);

      // Do the update
      mgmciter(u, S_g, mg_control.hbitr_params.hb_params, color); //one hb sweep

      // Do measurements
      doMeas(xml_out, u, mg_control, false, cur_update,
	     default_measurements, user_measurements);

      // Save if needed
      if( cur_update % mg_control.mc_control.save_interval == 0 ) 
      {
	saveState(mg_control.hbitr_params, mg_control.mc_control, 
		  cur_update,
		  mg_control.inline_measurement_xml, u);
      }

      pop(xml_out); // pop("Update");
      pop(xml_out); // pop("elem");
    }

    pop(xml_out); // pop("MCUpdates")
    
    END_CODE();
  }
  

  //--------------------------------------------------------------------------
  void doHB(multi1d<LatticeColorMatrix>& u,
	    const LinearGaugeAction& S_g,
	    MGControl& mg_control, 
	    multi1d< Handle<AbsInlineMeasurement> >& user_measurements,
            Set color) 
  {
    START_CODE();

    XMLWriter& xml_out = TheXMLOutputWriter::Instance();
    push(xml_out, "doHB");

    multi1d< Handle< AbsInlineMeasurement > > default_measurements(1);
    InlinePlaquetteEnv::Params plaq_params;
    plaq_params.frequency = 1;
    // It is a handle
    default_measurements[0] = new InlinePlaquetteEnv::InlineMeas(plaq_params);

    try 
    {
      // Initialise the RNG
      QDP::RNG::setrn(mg_control.mc_control.rng_seed);
      
      // If warmups are required, do them first
      if (mg_control.mc_control.n_warm_up_updates > 0)
      {
	doWarmUp(xml_out, u, S_g, mg_control, default_measurements, user_measurements,color);
	mg_control.mc_control.n_warm_up_updates = 0;  // reset
      }

      // Do the production updates
      doProd(xml_out, u, S_g, mg_control, default_measurements, user_measurements,color);
    }
    catch( const std::string& e) { 
      QDPIO::cerr << "Caught Exception: " << e << std::endl;
      QDP_abort(1);
    }

    pop(xml_out);
    
    END_CODE();
  }
  
  bool linkageHack(void)
  {
    bool foo = true;
    
    // Gauge actions
    foo &= GaugeActsEnv::registerAll();

    // Inline Measurements
    foo &= InlineAggregateEnv::registerAll();

    return foo;
  }



  // Returns LCM a shifted by dist steps
  // The use of this method as written is inefficient for higher l, in most cases dist = 2^l
  // Kostas teach me how to use maps to do this process in one jump instead of 2^l
  LatticeColorMatrix shifter(const LatticeColorMatrix& a, int sign, int dir, int dist){
    START_CODE();
    LatticeColorMatrix b;
    LatticeColorMatrix c;
    b=a;
    for(int i=0;i<dist;++i){
      c=shift(b,sign,dir);
      b=c;
    }
    return b;
    END_CODE();
  }

  // A method to coarsen u on level l-1 to level l
  void coarsen(multi1d<LatticeColorMatrix>& u, Set color)
  {
    START_CODE();
    // Calculate l from color's numsubsets and a little loop
    int t=(color.numSubsets()-1)/4+1;// t starts as 2^l
    int l=0;
    while(t >= 2){t>>1; l++;}

    for(int mu(0); mu<Nd;++mu)
    {  // Shifts links such that u[color[0]] and u2[color[0]] are the two to be combined.
      LatticeColorMatrix g;
      LatticeColorMatrix u2;
      LatticeColorMatrix temp;
  
      u2=1;// May be unnecessary
      u2[color[0]]=shifter(u[mu],FORWARD,mu,(1<<(l-1)));
 
      // Set the gauge transformation by shifting the temp
      g=1;
      temp=g;
      for(int i(0); i < 1<<(l-1); ++i)
      {
        g=shift(temp,FORWARD,mu);
        g[color[0]]=adj(u2);
        temp=g;
      }
      g=shifter(temp,BACKWARD,mu,(1<<(l-1)));
 
      // Do the gauge transformation
      for(int nu(0); nu < Nd; ++nu)
      {
        temp=u[nu];
        u[nu]=g*temp*adj(shift(g,FORWARD,nu));
      }
    }
    END_CODE();
  }

  // Refines lattice u from level l to l-1
  // Refines using defintion u1=V*d, u2=d*V. This may need to be changed to involve sqrt
  void finen(multi1d<LatticeColorMatrix>& u, multi1d<LatticeColorMatrix>& oldu, Set color){
    START_CODE();
    int t=(color.numSubsets()-1)/4+1;// t starts as 2^l
    int l=0;
    while(t >= 2){t>>1; l++;}
  
    for(int m=0; m<Nd; ++m){
      // Hold this for after undoing gauge transformation.
      LatticeColorMatrix v,u2,g,temp;
      v[color[0]]=u[m];
     
      u2[color[0]]=shifter(oldu[m],m,FORWARD,1<<(l-1));

      // Below is a copy of the gauge transformation in coarsen with the change that g=u2 not g=adj(u2)
      // Set the gauge transformation by shifting the temp
      g=1;
      temp=g;
      for(int i(0); i < 1<<(l-1); ++i)
      {
        g=shift(temp,FORWARD,m);
        g[color[0]]=u2;
        temp=g;
      }
      g=shifter(temp,BACKWARD,m,(1<<(l-1)));
 
      // Do the gauge transformation
      for(int nu(0); nu < Nd; ++nu)
      {
        temp=u[nu];
        u[nu]=g*temp*adj(shift(g,FORWARD,nu));
      }
    
      // Apply the fine and coarse link relations.
      u[m][color[0]]=v*adj(u2);
      temp[color[0]]=shifter(u[m],FORWARD,m,(1<<(l-1)));
      temp[color[0]]=adj(oldu[m])*v;
      u[m]=shifter(temp,BACKWARD,m,1<<(l-1));
    }
    END_CODE();
  }

  // coeffs are stored as global variables, so the functions can edit them without having to rewrite what is in the monomial xml
  //                                                            _
  multi2d<LatticeColorMatrix> a; // unextended corner shaped---  |
  multi2d<LatticeColorMatrix> b; // unextended corner shaped----- _|  _
  multi2d<LatticeColorMatrix> c; // unextended corner shaped-------- |
  multi2d<LatticeColorMatrix> d; // unextended corner shaped-- |_
  multi1d<LatticeColorMatrix> h; // coefficient for term linear in V

  // Calculated the coefficient matrices for the level l action using the level l-1 lattice u
  void actcoeff(multi1d<LatticeColorMatrix>& u, Set color){
    START_CODE();
    // Calculate l from color's numsubsets and a little loop
    int t=(color.numSubsets()-1)/4+1;// t starts as 2^l
    int l=0;
    while(t >= 2){t>>1; l++;}

    if(l==1){
      for(int mu=0;mu<Nd; ++mu){
        for(int nu=0;nu<Nd; ++nu){
          if(nu!=mu){
            // The first level with shifter.
            a[mu][nu][color[0]] = shift(adj(u[mu]),FORWARD,mu)*shift(u[nu],FORWARD,mu)*shift(adj(u[mu]),FORWARD,nu)*shift(u[nu],FORWARD,nu);//CHECKED for correct terms

            b[mu][nu][color[0]] = shift(shifter(adj(u[nu]),FORWARD,mu,2),FORWARD,nu)*shift(shift(adj(u[mu]),FORWARD,mu),FORWARD,nu)*shift(adj(u[nu]),FORWARD,mu)*adj(u[mu]);// CHECKED for correct terms
 
            c[mu][nu][color[0]] = shift(adj(u[mu]),FORWARD,mu)*shift(shift(adj(u[nu]),FORWARD,mu),BACKWARD,nu)*shift(adj(u[mu]),BACKWARD,nu)*shifter(adj(u[nu]),BACKWARD,nu,2);// CHECKED for correct terms

            d[mu][nu][color[0]] = shifter(shifter(u[nu],FORWARD,mu,2),BACKWARD,nu,2)*shift(shift(adj(u[mu]),FORWARD,mu),BACKWARD,nu)*shift(shift(u[nu],FORWARD,mu),BACKWARD,nu)*adj(u[mu]);// CHECKED for correct terms
          }
        } 
        h[mu][color[0]] = 0;// CHECKED for correct terms
      }
    
    }
    else if(l>1){// Not tested  
      for(int mu=0;mu<Nd; ++mu){
        LatticeColorMatrix temp,hsum;
        hsum=0; temp=0; // What would the default values be if I were to not initialize it like this?
        for(int nu=0;nu<Nd; ++nu){
          if(nu!=mu){
            temp[color[0]]=shifter(adj(u[mu]),FORWARD,mu,1<<(l-1))*(shifter(u[nu],FORWARD,mu,1<<(l-1))*b[mu][nu]+
		shifter(shifter(adj(u[nu]),FORWARD,mu,1<<(l-1)),BACKWARD,nu,1<<(l-1))*d[mu][nu])+
          	(shifter(a[mu][nu],FORWARD,mu,1<<(l-1))*shifter(adj(u[nu]),FORWARD,mu,1<<(l-1))+
		shifter(c[mu][nu],FORWARD,mu,1<<(l-1))*shifter(shifter(u[nu],FORWARD,mu,1<<(l-1)),BACKWARD,nu,1<<(l-1)))*adj(u[mu]) + hsum;//checked for correct terms
            hsum=temp;

            a[mu][nu][color[0]]=shifter(adj(u[mu]),FORWARD,mu,1<<(l-1))*a[mu][nu]*shifter(u[nu],FORWARD,nu,1<<(l-1));//checked for correct terms
            b[mu][nu][color[0]]=shifter(shifter(adj(u[nu]),FORWARD,mu,(1<<l)),FORWARD,nu,1<<(l-1))*shifter(b[mu][nu],FORWARD,mu,1<<(l-1))*adj(u[mu]);//checked for correct terms
            c[mu][nu][color[0]]=shifter(adj(u[mu]),FORWARD,mu,1<<(l-1))*c[mu][nu]*shifter(adj(u[nu]),BACKWARD,nu,1<<l);//checked for correct terms
            d[mu][nu][color[0]]=shifter(shifter(u[nu],FORWARD,mu,(1<<l)),BACKWARD,nu,1<<l)*shifter(d[mu][nu],FORWARD,mu,1<<(l-1))*adj(u[mu]);//checked for correct terms
          }
        }
        h[mu][color[0]]=hsum + shifter(adj(u[mu]),FORWARD,mu,1<<(l-1))*h[mu]+shifter(h[mu],FORWARD,mu,1<<(l-1))*adj(u[mu]);//checked for correct terms
      }
    }
    END_CODE();
  }


  void doMG(multi1d<LatticeColorMatrix>& u,
	    MGControl& mg_control, 
	    multi1d< Handle<AbsInlineMeasurement> >& the_measurements,
            Set color)  {
  
    // numSubsets()=4*((1<<l)-1)+1
    // Calculate l from color's numsubsets and a little loop
    int t=(color.numSubsets()-1)/4+1;// t starts as 2^l
    int l=0;
    while(t >= 2){t >> 1; l++;}

    // Save the old coeffs for later use
    // Hopefully will be made obsolete in a future version of this method
    multi2d<LatticeColorMatrix> olda(Nd,Nd);
    multi2d<LatticeColorMatrix> oldb(Nd,Nd);
    multi2d<LatticeColorMatrix> oldc(Nd,Nd);
    multi2d<LatticeColorMatrix> oldd(Nd,Nd);
    multi1d<LatticeColorMatrix> oldh(Nd);
    multi1d<LatticeColorMatrix> oldu(Nd);

    // This coeffs should somehow find their way into and xml to be passed to gauge_act
    //QUESTION: Does assignment require for loops for multi#d<LCM> or can I just do below?
    olda=a;
    oldb=b;
    oldc=c;
    oldd=d;
    oldh=h;
    /*for(int m=0; m<Nd; ++m){
      for(int n=0; n<Nd; ++n){
        olda[m][n]=a[m][n];
        oldb[m][n]=b[m][n];
        oldc[m][n]=c[m][n];
        oldd[m][n]=d[m][n];
      }
      oldh[m]=h[m];
      oldu[m]=u[m];
    }*/
    Handle< LinearGaugeAction > S_g;
    Handle< LinearGaugeAction > oldS_g;
    try
    {
      std::istringstream is(mg_control.hbitr_params.hb_gaugeact.gauge_act);
      XMLReader gaugeact_reader(is);
      XMLBufferWriter buf;
      buf<<gaugeact_reader;
      push(buf,"/GaugeAction");
      for(int m=0; m<Nd; ++m){
        push(buf,std::to_string(m));
        write(buf,"a",a[m]);
        write(buf,"b",b[m]);
        write(buf,"c",c[m]);
        write(buf,"d",d[m]);
        pop(buf);
      }
      write(buf,"h",h);
      write(buf,"l",l);
      pop(buf);
      mg_control.hbitr_params.hb_gaugeact.gauge_act= buf.str();

      std::istringstream is2(mg_control.hbitr_params.hb_gaugeact.gauge_act);
      XMLReader reader(is2);

      // Throw an exception if not found
      typedef multi1d<LatticeColorMatrix>  P;
      typedef multi1d<LatticeColorMatrix>  Q;


/*      Handle< CreateGaugeState<P,Q> > cgs_;
      cgs_ = CreateGaugeStateEnv::reader(reader, "/GaugeAction");
      MgGaugeActParams p MgGaugeActParams(reader, "/GaugeAction");*/
      LinearGaugeAction* gaugeact = new MgGaugeAct(reader, "/GaugeAction");//MgGaugeAct(cgs_,p);
      S_g = dynamic_cast<LinearGaugeAction*>(gaugeact);
      oldS_g=S_g;
    }
    catch(std::bad_cast) 
    {
      QDPIO::cerr << "PURGAUG: caught cast error" << std::endl;
      QDP_abort(1);
    }  
    catch(std::bad_alloc) 
    { 
      // This might happen on any node, so report it
      std::cerr << "PURGAUG: caught bad memory allocation" << std::endl;
      QDP_abort(1);
    }
    catch(const std::string& e) 
    {
      QDPIO::cerr << "PURGAUG: Caught Exception: " << e << std::endl;
      QDP_abort(1);
    }
    catch(std::exception& e) 
    {
      QDPIO::cerr << "PURGAUG: Caught standard library exception: " << e.what() << std::endl;
      QDP_abort(1);
    }
    catch(...)
    {
      // This might happen on any node, so report it
      std::cerr << "PURGAUG: caught generic exception during measurement" << std::endl;
      QDP_abort(1);
    }

    for(int i=0; i<mg_control.npre; ++i){ doHB(u, *S_g, mg_control, the_measurements, color);}

    if(l<mg_control.lmax){
    
      // calculate coefficients and coarsen lattice
      actcoeff(u,color);
      coarsen(u,color);
      color.make(ColorFunc(l+1));

      // I have a plan to change how the writing and storage of coeffs works
      // the global coeffs will not be needed, they will be stored and read from the monomial
      // actcoeff will do the writing to the monomial, then oldcoeffs will be stored
      // after mg call oldcoeffs will be written into monomial\
      //  but I am fairly sure the below will work for now so I will make this edit later
      
      // call recursive update (gam=1 is V cycle, gam=2 is W cycle)
      for(int g=0; g<mg_control.gam; ++g) doMG(u, mg_control, the_measurements,color);

      // refine the lattice from the updated coarse lattice
      finen(u,oldu,color);
      color.make(ColorFunc(l));
    
      // reset the coeffs for post update
      a=olda;
      b=oldb;
      c=oldc;
      d=oldd;
      h=oldh;
      S_g=oldS_g;
    }
    for(int i=0; i<mg_control.npost; ++i){ doHB(u, *S_g, mg_control, the_measurements, color);}
  }
};


using namespace Chroma;

//! Pure gauge field generation via heatbath
/*! \defgroup purgaug Heat-bath
 *  \ingroup main
 *
 * Main program for heat-bath generation of 
 */

int main(int argc, char *argv[]) 
{
  Chroma::initialize(&argc, &argv);
  
  START_CODE();

  // Chroma Init stuff -- Open DATA and XMLDAT
  linkageHack();

  XMLFileWriter& xml_out = Chroma::getXMLOutputInstance();
  push(xml_out, "purgaug");

  MGControl  mg_control;

  try
  {
    XMLReader xml_in(Chroma::getXMLInputFileName());

    read(xml_in, "/purgaug", mg_control);

    // Write out the input
    write(xml_out, "Input", xml_in);
  }
  catch( const std::string& e ) {
    QDPIO::cerr << "Caught Exception reading input XML: " << e << std::endl;
    QDP_abort(1);
  }
  catch( std::exception& e ) {
    QDPIO::cerr << "Caught standard library exception: " << e.what() << std::endl;
    QDP_abort(1);
  }
  catch(...) {
    QDPIO::cerr << "Caught unknown exception " << std::endl;
    QDP_abort(1);
  }

  Layout::setLattSize(mg_control.hbitr_params.nrow);
  Layout::create();

  proginfo(xml_out);    // Print out basic program info

  // Start up the config
  multi1d<LatticeColorMatrix> u(Nd);
  {
    XMLReader file_xml;
    XMLReader config_xml;
    
    gaugeStartup(file_xml, config_xml, u, mg_control.cfg);

    // Write out the config header
    push(xml_out, "Config_info");
    write(xml_out, "file_xml", file_xml);
    write(xml_out, "config_xml", config_xml);
    pop(xml_out);
  }
  
  // The create gauge action code was pushed till during the MG update
  // Create the gauge action
  // This code is limited to only rb sets and subsets.
  // The main point is
  // The number of subsets within the staples, etc. and to get the subsets
  // straight
  /*
  Handle< LinearGaugeAction > S_g;
  try
  {
    std::istringstream is(mg_control.hbitr_params.hb_gaugeact.gauge_act);
    XMLReader gaugeact_reader(is);

    // Get the name of the gauge act
    std::string gaugeact_string;
    try { 
      read(gaugeact_reader, "/GaugeAction/Name", gaugeact_string);
    }
    catch( const std::string& e) 
    {
      QDPIO::cerr << "Error grepping the gaugeact name: " << e<<  std::endl;
      QDP_abort(1);
    }

    // Throw an exception if not found
    typedef multi1d<LatticeColorMatrix>  P;
    typedef multi1d<LatticeColorMatrix>  Q;

    GaugeAction<P,Q>* gaugeact = 
      TheGaugeActFactory::Instance().createObject(gaugeact_string, 
						  gaugeact_reader, 
						  "/GaugeAction");
    S_g = dynamic_cast<LinearGaugeAction*>(gaugeact);
  }
  catch(std::bad_cast) 
  {
    QDPIO::cerr << "PURGAUG: caught cast error" << std::endl;
    QDP_abort(1);
  }
  catch(std::bad_alloc) 
  { 
    // This might happen on any node, so report it
    std::cerr << "PURGAUG: caught bad memory allocation" << std::endl;
    QDP_abort(1);
  }
  catch(const std::string& e) 
  {
    QDPIO::cerr << "PURGAUG: Caught Exception: " << e << std::endl;
    QDP_abort(1);
  }
  catch(std::exception& e) 
  {
    QDPIO::cerr << "PURGAUG: Caught standard library exception: " << e.what() << std::endl;
    QDP_abort(1);
  }
  catch(...)
  {
    // This might happen on any node, so report it
    std::cerr << "PURGAUG: caught generic exception during measurement" << std::endl;
    QDP_abort(1);
  }
  */

  // Get the measurements
  multi1d < Handle< AbsInlineMeasurement > > the_measurements;

  try { 
    std::istringstream Measurements_is(mg_control.inline_measurement_xml);

    XMLReader MeasXML(Measurements_is);

    std::ostringstream os;
    MeasXML.print(os);
    QDPIO::cout << os.str() << std::endl << std::flush;

    read(MeasXML, "/InlineMeasurements", the_measurements);
  }
  catch(const std::string& e) { 
    QDPIO::cerr << "Caugth exception while reading measurements: " << e << std::endl
		<< std::flush;

    QDP_abort(1);
  }

  QDPIO::cout << "There are " << the_measurements.size() << " user measurements " << std::endl;

  
  // Run
  try 
  {

    Set color;
    color.make(ColorFunc(0));
    doMG(u, mg_control, the_measurements,color);
    //doHB(u, *S_g, mg_control, the_measurements);
  } 
  catch(std::bad_cast) 
  {
    QDPIO::cerr << "PURGAUG: caught cast error" << std::endl;
    QDP_abort(1);
  }
  catch(std::bad_alloc) 
  { 
    QDPIO::cerr << "PURGAUG: caught bad memory allocation" << std::endl;
    QDP_abort(1);
  }
  catch(const std::string& e) 
  { 
    QDPIO::cerr << "PURGAUG: Caught std::string exception: " << e << std::endl;
    QDP_abort(1);
  }
  catch(std::exception& e) 
  {
    QDPIO::cerr << "PURGAUG: Caught standard library exception: " << e.what() << std::endl;
    QDP_abort(1);
  }
  catch(...) 
  {
    QDPIO::cerr << "PURGAUG: Caught generic/unknown exception" << std::endl;
    QDP_abort(1);
  }

  pop(xml_out);

  END_CODE();

  Chroma::finalize();
  exit(0);
}

