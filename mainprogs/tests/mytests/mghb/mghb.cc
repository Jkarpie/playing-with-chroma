/*! \file
 *  \brief Main code for pure gauge field generation
 */

#include "chroma.h"
#include "actions/gauge/gaugeacts/gaugeacts_aggregate.h"
#include <qdp-lapack.h>
#include <stdlib.h>
#include <time.h>

using namespace Chroma;


// A coloring function to find the active sites on the coarse lattice (when color = 0).
class ColorFunc : public SetFunc{
private:
  int l ;
public:
  ColorFunc(int l_):l(l_)  {}
  
  int operator()(const multi1d<int>& coord) const {
    int a=0;
    for(int mu=0; mu < Nd; ++mu){a+=coord[mu]%(1<<l);}
    return a;
  }
  int numSubsets() const {
    return 4*((1<<l)-1)+1;// ex: for l=1, the possible values are 0, 1, 2, 3, 4
  }  
} ;
class ActionParam
  {
 
  public:
    int l;
    //                                                                 _
    multi2d<LatticeColorMatrix> a; // unextended corner shaped like---  |
    multi2d<LatticeColorMatrix> b; // unextended corner shaped like----- _|  _
    multi2d<LatticeColorMatrix> c; // unextended corner shaped like-------- |
    multi2d<LatticeColorMatrix> d; // unextended corner shaped like-- |_
    multi1d<LatticeColorMatrix> h; // coefficient for term linear in V
    void setl(int l_){ l=l_;}
    ActionParam()
    {
      l=0;
      a.resize(Nd,Nd);  b.resize(Nd,Nd); c.resize(Nd,Nd); d.resize(Nd,Nd); h.resize(Nd);
      a=1; b=1; c=1; d=1; h=1;
    } 
  };


namespace Chroma 
{

  //! Holds gauge action
  struct HBGauge
  {
    std::string  gauge_act;     /*!<  Holds gauge action xml */
  };


  //! Read the parameters
  void read(XMLReader& xml_in, const std::string& path, HBGauge& p)
  {
    try {
      // Read the inverter Parameters
      XMLReader xml_tmp(xml_in, "./GaugeAction");
      std::ostringstream os;
      xml_tmp.print(os);
      p.gauge_act = os.str();
    }
    catch(const std::string& s) {
      QDPIO::cerr << "Caught Exception while reading gauge action: " << s <<std::endl;
      QDP_abort(1);
    }

    QDPIO::cout << "Gauge action: read \n" << p.gauge_act << std::endl;
  }


  //! Writer
  void write(XMLWriter& xml, const std::string& path, const HBGauge& p)
  {
    xml << p.gauge_act;
  }


  //! Reader
  void read(XMLReader& xml, const std::string& path, HBParams& p)
  {
    try { 
      XMLReader paramtop(xml, path);
      read(paramtop, "NmaxHB", p.NmaxHB);
      read(paramtop, "nOver", p.nOver);
    }
    catch(const std::string& e ) { 
      QDPIO::cerr << "Caught Exception reading HBParams: " << e << std::endl;
      QDP_abort(1);
    }
  }

  //! Writer
  void write(XMLWriter& xml, const std::string& path, const HBParams& p)
  {
    push(xml, path);

    write(xml, "NmaxHB", p.NmaxHB);
    write(xml, "nOver", p.nOver);

    pop(xml);
  }


  //! Params controlling running of monte carlo
  struct MCControl 
  {
    QDP::Seed rng_seed;
    unsigned long start_update_num;
    unsigned long n_warm_up_updates;
    unsigned long n_production_updates;
    unsigned int  n_updates_this_run;
    unsigned int  save_interval;
    std::string   save_prefix;
    QDP_volfmt_t  save_volfmt;
  };

  void read(XMLReader& xml, const std::string& path, MCControl& p) 
  {
    try { 
      XMLReader paramtop(xml, path);
      read(paramtop, "./RNG", p.rng_seed);
      read(paramtop, "./StartUpdateNum", p.start_update_num);
      read(paramtop, "./NWarmUpUpdates", p.n_warm_up_updates);
      read(paramtop, "./NProductionUpdates", p.n_production_updates);
      read(paramtop, "./NUpdatesThisRun", p.n_updates_this_run);
      read(paramtop, "./SaveInterval", p.save_interval);
      read(paramtop, "./SavePrefix", p.save_prefix);
      read(paramtop, "./SaveVolfmt", p.save_volfmt);

      if (p.n_updates_this_run % p.save_interval != 0)
	throw std::string("UpdateThisRun not a multiple of SaveInterval");
    }
    catch(const std::string& e ) { 
      QDPIO::cerr << "Caught Exception reading MCControl: " << e << std::endl;
      QDP_abort(1);
    }
  }

  void write(XMLWriter& xml, const std::string& path, const MCControl& p) 
  {
    push(xml, path);

    write(xml, "RNG", p.rng_seed);
    write(xml, "StartUpdateNum", p.start_update_num);
    write(xml, "NWarmUpUpdates", p.n_warm_up_updates);
    write(xml, "NProductionUpdates", p.n_production_updates);
    write(xml, "NUpdatesThisRun", p.n_updates_this_run);
    write(xml, "SaveInterval", p.save_interval);
    write(xml, "SavePrefix", p.save_prefix);
    write(xml, "SaveVolfmt", p.save_volfmt);

    pop(xml);
  }

 /* struct ActionParam
  {

    int l;
    //                                                                 _
    multi2d<LatticeColorMatrix> a; // unextended corner shaped like---  |
    multi2d<LatticeColorMatrix> b; // unextended corner shaped like----- _|  _
    multi2d<LatticeColorMatrix> c; // unextended corner shaped like-------- |
    multi2d<LatticeColorMatrix> d; // unextended corner shaped like-- |_
    multi1d<LatticeColorMatrix> h; // coefficient for term linear in V

    void initialize()
    {
      l=0;
      a.resize(Nd,Nd);  b.resize(Nd,Nd); c.resize(Nd,Nd); d.resize(Nd,Nd); h.resize(Nd);
      a=1; b=1; c=1; d=1; h=1;
    }    
  };*/


  //--------------------------------------------------------------------------

  //! Holds params for Heat-bath
  struct HBItrParams 
  { 
    multi1d<int> nrow;
    // New params for multigrid control
    unsigned int  npre;
    unsigned int  npost;
    unsigned int  gam;
    unsigned int lmax;

    HBGauge   hb_gaugeact;    /*!< This is polymorphic */
    HBParams  hb_params;      /*!< Solely the HB bit */
  };

  void write(XMLWriter& xml, const std::string& path, const HBItrParams& p)
  {
    push(xml, path);
    write(xml, "nrow", p.nrow);

    write(xml, "npre", p.npre);
    write(xml, "npost", p.npost);
    write(xml, "gam", p.gam);
    write(xml, "lmax", p.lmax);

    write(xml, "GaugeAction", p.hb_gaugeact);
    write(xml, "HBParams", p.hb_params);
    pop(xml);
  }


  void read(XMLReader& xml, const std::string& path, HBItrParams& p) 
  {
    try {
      XMLReader paramtop(xml, path);
      
      read(paramtop, "nrow", p.nrow);
      read(paramtop, "npre", p.npre);
      read(paramtop, "npost", p.npost);
      read(paramtop, "gam", p.gam);
      read(paramtop, "lmax", p.lmax);

      read(paramtop, "GaugeAction", p.hb_gaugeact);
      read(paramtop, "HBParams", p.hb_params);
    }
    catch( const std::string& e ) { 
      QDPIO::cerr << "Error reading HBItrParams XML : " << e << std::endl;
      QDP_abort(1);
    }
  }

  //! Main struct from input params and output restarts
  struct MGControl 
  {
    HBItrParams   hbitr_params;
    MCControl     mc_control;
    Cfg_t         cfg;
    std::string   inline_measurement_xml;
  };


  //! Reader
  void read(XMLReader& xml_in, const std::string& path, MGControl& p) 
  {
    try {
      XMLReader paramtop(xml_in, path);

      read(paramtop, "HBItr", p.hbitr_params);
      read(paramtop, "MCControl", p.mc_control);
      read(paramtop, "Cfg", p.cfg);

      if( paramtop.count("./InlineMeasurements") == 0 ) {
	XMLBufferWriter dummy;
	push(dummy, "InlineMeasurements");
	pop(dummy); // InlineMeasurements
	p.inline_measurement_xml = dummy.printCurrentContext();
      }
      else 
      {
	XMLReader measurements_xml(paramtop, "./InlineMeasurements");
	std::ostringstream inline_os;
	measurements_xml.print(inline_os);
	p.inline_measurement_xml = inline_os.str();
	QDPIO::cout << "InlineMeasurements are: " << std::endl;
	QDPIO::cout << p.inline_measurement_xml << std::endl;
      }
    }
    catch(const std::string& e) {
      QDPIO::cerr << "Caught Exception reading MGControl: " << e << std::endl;
      QDP_abort(1);
    }
  }


  //! Writer
  void write(XMLWriter& xml, const std::string& path, const MGControl& p) 
  {
    push(xml, path);

    write(xml, "Cfg", p.cfg);
    write(xml, "MCControl", p.mc_control);
    xml << p.inline_measurement_xml;
    write(xml, "HBItr", p.hbitr_params);

    pop(xml);
  }



  //--------------------------------------------------------------------------
  // Specialise
  MCControl newMCHeader(const HBItrParams& update_params, 
			const MCControl& mc_control,
			unsigned long update_no)
  {
    START_CODE();

    // Copy old params
    MCControl p_new = mc_control;
    
    // Get Current RNG Seed
    QDP::RNG::savern(p_new.rng_seed);
   
    // Set the current traj number
    p_new.start_update_num = update_no;
    
    // Reset the warmups
    p_new.n_warm_up_updates = 0;
    
    // Set the num_updates_this_run
    unsigned long total = mc_control.n_production_updates;

    if ( total < mc_control.n_updates_this_run + update_no ) { 
      p_new.n_updates_this_run = total - update_no;
    }

    END_CODE();

    return p_new;
  }

  //--------------------------------------------------------------------------


  // Specialise
  void saveState(const HBItrParams& update_params, 
		 MCControl& mc_control,
		 unsigned long update_no,
		 const std::string& inline_measurement_xml,
		 const multi1d<LatticeColorMatrix>& u)
  {
    START_CODE();

    MCControl mc_new = newMCHeader(update_params, mc_control, update_no);

    // Files
    std::ostringstream restart_data_filename;
    std::ostringstream restart_config_filename;

    unsigned long save_num = update_no / mc_control.save_interval;
    restart_data_filename << mc_control.save_prefix << ".ini.xml" << save_num;
    restart_config_filename << mc_control.save_prefix << ".lime" << save_num;
    
    {
      MGControl hb;
      hb.hbitr_params = update_params;
      hb.mc_control = mc_new;
      hb.inline_measurement_xml = inline_measurement_xml;

      // Set the name of the restart file
      hb.cfg.cfg_file = restart_config_filename.str();

      // Hijack this for now and assumes it means what I want it to mean
      hb.cfg.cfg_type = CFG_TYPE_SZINQIO;

      // Write a restart DATA file from the buffer XML
      XMLFileWriter restart_xml(restart_data_filename.str().c_str());
      write(restart_xml, "purgaug", hb);
      restart_xml.close();
    }

    {
      // Save the config

      // some dummy header for the file
      XMLBufferWriter file_xml;
      push(file_xml, "HB");
      proginfo(file_xml);
      pop(file_xml);

      XMLBufferWriter config_xml;
      push(config_xml, "ChromaHB");
      write(config_xml, "MCControl", mc_new);
      write(config_xml, "HBItr", update_params);
      pop(config_xml);

      // Save the config
      writeGauge(file_xml, 
		 config_xml,
		 u,
		 restart_config_filename.str(),
		 mc_new.save_volfmt,
		 QDPIO_SERIAL);    
    }
    
    END_CODE();
  }


  //--------------------------------------------------------------------------


  void doMeas(XMLWriter& xml_out,
	      multi1d<LatticeColorMatrix>& u,
	      MGControl& mg_control, 
	      bool warm_up_p,
	      unsigned long cur_update,
	      const multi1d< Handle< AbsInlineMeasurement > >& default_measurements,
	      const multi1d< Handle<AbsInlineMeasurement> >& user_measurements) 
  {
    START_CODE();

    // Create a gauge header for inline measurements.
    // Since there are defaults always measured, we must always
    // create a header.
    //
    // NOTE: THIS HEADER STUFF NEEDS A LOT MORE THOUGHT
    //
    MCControl mc_new = newMCHeader(mg_control.hbitr_params, mg_control.mc_control, cur_update);

    XMLBufferWriter gauge_xml;
    push(gauge_xml, "ChromaHB");
    write(gauge_xml, "MCControl", mc_new);
    write(gauge_xml, "HBItr", mg_control.hbitr_params);
    pop(gauge_xml);

    // Reset and set the default gauge field
    InlineDefaultGaugeField::reset();
    InlineDefaultGaugeField::set(u, gauge_xml);

    // Measure inline observables 
    push(xml_out, "InlineObservables");

    // Always measure defaults
    for(int m=0; m < default_measurements.size(); m++) 
    {
      // Caller writes elem rule 
      AbsInlineMeasurement& the_meas = *(default_measurements[m]);
      push(xml_out, "elem");
      the_meas(cur_update, xml_out);
      pop(xml_out);
    }
	
    // Only measure user measurements after warm up
    if( ! warm_up_p ) 
    {
      QDPIO::cout << "Doing " << user_measurements.size() 
		  <<" user measurements" << std::endl;
      for(int m=0; m < user_measurements.size(); m++) 
      {
	AbsInlineMeasurement& the_meas = *(user_measurements[m]);
	if( cur_update % the_meas.getFrequency() == 0 ) 
	{ 
	  // Caller writes elem rule
	  push(xml_out, "elem");
	  the_meas(cur_update, xml_out );
	  pop(xml_out); 
	}
      }
    }
    pop(xml_out); // pop("InlineObservables");

    // Reset the default gauge field
    InlineDefaultGaugeField::reset();
    
    END_CODE();
  } 


  //--------------------------------------------------------------------------


  // returns the sign of t
  Int sign(Real t)
  {
    START_CODE();
    Int ans = Int(Real(0) < t) - Int(Real(0) > t);

    // In the case of phi=0/pi, ans= 0 not +1/-1, this will always map to phi to pi,
    // This if statement sets phi to 0 instead.
    if(toBool(ans!=Int(0))) return ans;
    else return Int(1);
    END_CODE();
  }


  //--------------------------------------------------------------------------


  // returns z^n For fractional n, returns z^n = r^n e^(i n phi) where phi is [0,2pi]
  Complex power(Complex z, Real n)
  {
    START_CODE();
    Real r(pow(real(z)*real(z)+imag(z)*imag(z),Real(.5)));
    Real phi(acos(real(z)/r));
    Int sgn(sign(imag(z)));
    Real temp=phi;
    phi=temp + (sgn-Int(1))*(temp-Real(3.141592));
    END_CODE();

    return cmplx(pow(r,n)*cos(phi*n),pow(r,n)*sin(phi*n));
  }


  //--------------------------------------------------------------------------

  // Finds the SU(3) matrices which are the sqrt of u
  LatticeColorMatrix sqrt(LatticeColorMatrix& u, Set color)
  {
    START_CODE();
    int t=(color.numSubsets()-1)/4+1;// t starts as 2^l
    int l=0;
    while(t >= 2){t=t>>1; l++;}

    LatticeColorMatrix sq=1;
    multi1d<int> x(Nd);
    for(int i=0;i<Nd;i++) x[i]=0;
    multi1d<int> nrow(Nd);

    nrow = Layout::lattSize();
    int stepsize=1<<l;

    while(x[0] < nrow[0]){
      while(x[1] < nrow[1]){
        while(x[2] < nrow[2]){
          while(x[3] < nrow[3]){ 
            ColorMatrix sqrtu;
  
            multi2d<DComplex> mat;
            mat.resize(Nc,Nc);
  
            ColorMatrix v = peekSite(u,x);
            ColorVector halfevtemp;
            ColorVector temp;

	    // Fill the matrix for lapack
            for(int i=0; i<Nc; i++)
            {
              for(int j=0; j<Nc; j++)
              {
                mat[i][j]=peekColor(v,i,j);
              }
            }

            multi1d<DComplex> values;
            values.resize(Nc);
            multi2d<DComplex> vectors;
            vectors.resize(Nc,Nc);

            // Find evalues and evectors
            QDPLapack::zgeev(Nc, mat, values, vectors);

            // Fill CMs with evalue^.5 diagonal and evector columns
	    ColorMatrix l=1;
            ColorMatrix s=1;

  	    Complex det(cmplx(Real(1),Real(0)));
            for(int i=0; i<Nc; i++)
            {

              temp = pokeColor(temp,(Complex) values[i], i);

              for(int j=0; j<Nc; j++)
              {
                //l = pokeColor(l, cmplx(Real(0),Real(0)) ,i,j); //if l is set to 1 then all off diag are 0
                Complex vec((Complex) vectors[i][j]);
                s = pokeColor(s, vec,i,j);
              }
              Complex eval(power((Complex) values[i],Real(.5)));
	      det *= eval;
              l = pokeColor(l, (Complex) eval, i, i);
            }
            sqrtu = adj(s)*l*s;
	  
	    if(toBool(real(det)<Real(0))) sqrtu*=-1;

  	    sq = pokeSite(sq,sqrtu,x);  
            x[3] += stepsize;
          }
	  x[3] = 0;
          x[2] += stepsize;
        }
        x[2] = 0;
        x[1] += stepsize;
      }
      x[1] = 0;
      x[0] += stepsize;
    }

    END_CODE();
    return sq;
  }


  //--------------------------------------------------------------------------


  // Returns LCM a shifted by dist steps
  // The use of this method as written is inefficient for higher l, in most cases dist = 2^l
  LatticeColorMatrix shifter(const LatticeColorMatrix& a,
                             int sign, int dir, int dist){
    START_CODE();
    LatticeColorMatrix b;
    LatticeColorMatrix c;
    b=a;
    for(int i=0;i<dist;++i){
      c=shift(b,sign,dir);
      b=c;
    }
    return b;
    END_CODE();
  }


  //--------------------------------------------------------------------------


  // A method to coarsen u on level l-1 to level l
  void coarsen(multi1d<LatticeColorMatrix>& u,
               multi1d<LatticeColorMatrix>& delta, 
               multi1d<LatticeColorMatrix>& g, 
               Set color)
  {
    START_CODE();
 
    int t=(color.numSubsets()-1)/4+1;// t starts as 2^l
    int l=0;
    while(t >= 2){t=t>>1; l++;}

  
    for(int mu(0); mu<Nd; mu++)
    {  // Shifts links such that u[color[0]] and u2[color[0]] are the two to be combined.
      LatticeColorMatrix u2=1;
      LatticeColorMatrix temp=1;
      u2[color[0]]=shifter(u[mu],FORWARD,mu,(1<<(l-1)));
      delta[mu]=1;
      g[mu]=1;

 
      // Set the gauge transformation by shifting the temp
      temp[color[0]]= u[mu]*u2;

 /* std::ostringstream os15;
  os15<<"sqrtv"<<"level"<<l<<"dir"<<mu<<".out.xml";

  XMLFileWriter xml_out15(os15.str());
  push(xml_out15,"LEVEL");
  write(xml_out15,"v",sqrt(temp,color));
  pop(xml_out15);
  xml_out15.close();*/


      g[mu][color[0]]=adj(sqrt(temp,color))*u[mu];
      temp=g[mu];
      g[mu]=shift(temp,BACKWARD,mu);


  /*std::ostringstream os;
  os<<"gauge"<<"level"<<l<<"dir"<<mu<<".out.xml";

  XMLFileWriter xml_out2(os.str());
  push(xml_out2,"LEVEL");
  write(xml_out2,"gauge",g[mu]);
  pop(xml_out2);
  xml_out2.close();*/

      // Do the gauge transformation
      for(int nu(0); nu < Nd; nu++)
      {
        temp=u[nu];
        u[nu]=g[mu]*temp*adj(shift(g[mu],FORWARD,nu));
      }
      delta[mu][color[0]]=u[mu]*adj(u2);
    }
    END_CODE();
  }

  //--------------------------------------------------------------------------

  void finen(multi1d<LatticeColorMatrix>& u,
             multi1d<LatticeColorMatrix>& oldu,
             Set color,
             multi1d<LatticeColorMatrix>& delta,
             multi1d<LatticeColorMatrix>& g)
  {
    START_CODE();
    int t=(color.numSubsets()-1)/4+1;// t starts as 2^l
    int l=0;
    while(t >= 2){t=t>>1; l++;}

    // This loop will undo the gauge trasformation that was applied during coarsening, then set u to new values using delta
    for(int m=0; m<Nd; m++){
      LatticeColorMatrix v=1;
      LatticeColorMatrix u2=1;
      LatticeColorMatrix temp=1;

      // Hold this for after gauge transformation is undone.
      v[color[0]] = u[m];
 
      // old u2 is usually stored on the lattice, except for the case of l=1
      u2[color[0]]=shifter(oldu[m],FORWARD,m,(1<<(l-1)));
  
      // Undo the gauge transformation
      for(int nu(0); nu < Nd; nu++)
      {
        temp = u[nu];
        u[nu] = adj(g[m])*temp*shift(g[m],FORWARD,nu);
      }
           
      // Apply the fine/coarse link relation
      u[m][color[0]] = v*delta[m];
      temp = shifter(u[m],FORWARD,m,(1<<(l-1)));
      temp[color[0]] = adj(delta[m])*v;
      u[m] = shifter(temp,BACKWARD,m,1<<(l-1));

    }
    END_CODE();
  }


  //--------------------------------------------------------------------------


  // Calculated the coefficient matrices for the action
  void actcoeff(multi1d<LatticeColorMatrix>& u,
                multi1d<LatticeColorMatrix>& delta, 
                Set color) //,           ActionParam& act)  
  {
    START_CODE();
    int t=(color.numSubsets()-1)/4+1;// t starts as 2^l
    int l=0;
    while(t >= 2){t=t>>1; l++;}

    // Do I need to manipulating the xml files here?
    try
    {
      TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> > ("Actiona");
      TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> > ("Actionb");
      TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> > ("Actionc");
      TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> > ("Actiond");
      TheNamedObjMap::Instance().getData< multi1d<LatticeColorMatrix> > ("Actionh");
    }
    catch( std::bad_cast ) 
    {
      QDPIO::cerr << "MGHB" << ": caught dynamic cast error" 
		  << std::endl;
      QDP_abort(1);
    }
    catch (const std::string& e) 
    {
      QDPIO::cerr << "MGHB" << ": std::map call failed: " << e 
		  << std::endl;
      QDP_abort(1);
    }

    multi2d<LatticeColorMatrix>& a = 
      TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> > ("Actiona");
    multi2d<LatticeColorMatrix>& b = 
      TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> > ("Actionb");
    multi2d<LatticeColorMatrix>& c = 
      TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> > ("Actionc");
    multi2d<LatticeColorMatrix>& d = 
      TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> > ("Actiond");
    multi1d<LatticeColorMatrix>& h = 
      TheNamedObjMap::Instance().getData< multi1d<LatticeColorMatrix> > ("Actionh");

    if(l==0){
      // It doesn't like resizing,
      a.resize(Nd,Nd); a=1;
      b.resize(Nd,Nd); b=1;
      c.resize(Nd,Nd); c=1;
      d.resize(Nd,Nd); d=1; 
      h.resize(Nd); h=1;
    }
    else if(l==1){
      for(int mu=0;mu<Nd; mu++)
      {
        for(int nu=0;nu<Nd; nu++)
        {
          if(nu!=mu)
          {
            a[mu][nu][color[0]] =delta[mu]*shift(u[nu],FORWARD,mu)*shift(adj(u[mu]),FORWARD,nu)*adj(delta[nu]);//CHECKED for correct terms

            b[mu][nu][color[0]] = shifter(delta[nu],FORWARD,mu,2)*shift(shift(adj(u[mu]),FORWARD,mu),FORWARD,nu)*shift(adj(u[nu]),FORWARD,mu)*adj(delta[mu]);// CHECKED for correct terms

            c[mu][nu][color[0]] = delta[mu]*shift(shift(adj(u[nu]),FORWARD,mu),BACKWARD,nu)*shift(adj(u[mu]),BACKWARD,nu)*shifter(adj(delta[nu]),BACKWARD,nu,2);// CHECKED for correct terms

            d[mu][nu][color[0]] = shifter(shifter(delta[nu],FORWARD,mu,2),BACKWARD,nu,2)*shift(shift(adj(u[mu]),FORWARD,mu),BACKWARD,nu)*shift(shift(u[nu],FORWARD,mu),BACKWARD,nu)*adj(delta[mu]);// CHECKED for correct terms
          }
        }// end nu

        h[mu][color[0]] = 0;// CHECKED for correct terms
      }// end mu
    
    }
    else if(l>1){    
      for(int mu=0;mu<Nd; mu++)
      {
        LatticeColorMatrix temp,hsum;
        hsum=0; temp=0;

        for(int nu=0;nu<Nd; nu++){
          if(nu != mu){	  
            hsum[color[0]] += delta[mu]*(shifter(u[nu],FORWARD,mu,1<<(l-1))*b[mu][nu] + shifter(shifter(adj(u[nu]),FORWARD,mu,1<<(l-1)),BACKWARD,nu,1<<(l-1))*d[mu][nu]) +
	    (shifter(a[mu][nu],FORWARD,mu,1<<(l-1))*shifter(adj(u[nu]),FORWARD,mu,1<<(l-1))+shifter(c[mu][nu],FORWARD,mu,1<<(l-1))*shifter(shifter(u[nu],FORWARD,mu,1<<(l-1)),BACKWARD,nu,1<<(l-1)))*adj(delta[mu]);//checked for correct terms

	    temp = a[mu][nu];
            a[mu][nu][color[0]] =delta[mu]*temp*adj(delta[nu]);//CHECKED for correct terms

            temp = shifter(b[mu][nu],FORWARD,mu,1<<(l-1));
            b[mu][nu][color[0]] = shifter(delta[nu],FORWARD,mu,1<<(l))*temp*adj(delta[mu]);// CHECKED for correct terms

            temp = c[mu][nu];
            c[mu][nu][color[0]] = delta[mu]*temp*shifter(adj(delta[nu]),BACKWARD,nu,1<<(l));// CHECKED for correct terms

            temp = shifter(d[mu][nu],FORWARD,mu,1<<(l-1));
            d[mu][nu][color[0]] = shifter(shifter(delta[nu],FORWARD,mu,1<<(l)),BACKWARD,nu,1<<(l))*temp*adj(delta[mu]);// CHECKED for correct terms
          }
        }
        temp = h[mu];
        h[mu][color[0]] = hsum + delta[mu]*temp+shifter(temp,FORWARD,mu,1<<(l-1))*adj(delta[mu]);//checked for correct terms
      }
    }
  }
// Finds the action from the fine plaquettes along the lth level coarse links
Double fineact(multi1d<LatticeColorMatrix>& u,int l){
  START_CODE();
  
  Double S_pg = zero;
  multi1d<LatticeColorMatrix> tempu(Nd);

  Set color;
  color.make(ColorFunc(l));
  // Compute the plaquettes
  for(int m=0; m < Nd; ++m){

    // This finds the plaquettes about color=0 sites
    for(int n=m+1; n < Nd; ++n){
      LatticeColorMatrix tempL;
      tempL=0;
      tempL[color[0]]=
		u[m]*shift(u[n],FORWARD,m)*shift(adj(u[m]),FORWARD,n)*adj(u[n])+ 
		u[m]*shift(shift(adj(u[n]),BACKWARD,n),FORWARD,m)*shift(adj(u[m]),BACKWARD,n)*shift(u[n],BACKWARD,n)+
		shift(u[m],BACKWARD,m)*u[n]*shift(shift(adj(u[m]),BACKWARD,m),FORWARD,n)*shift(adj(u[n]),BACKWARD,m)+
		shift(u[m],BACKWARD,m)*shift(adj(u[n]),BACKWARD,n)*shift(shift(adj(u[m]),BACKWARD,m),BACKWARD,n)*shift(shift(u[n],BACKWARD,m),BACKWARD,n);// The 4 plaquettes in the n,m plane about the color=0 site
      Double temp=sum(real(trace(tempL)));

      S_pg+=temp;
    }

    tempu=u;
    // This finds the action between all corner plaquettes
    for(int i=0; i < (1<<l)-2; ++i){
      //shifting takes a some of loops
      multi1d<LatticeColorMatrix> shiftee(Nd);
      for(int p=0; p < Nd; ++p) shiftee[p]=shift(tempu[p],FORWARD,m);
      tempu=shiftee;
      for(int n=0; n < Nd; ++n){
        if(n!=m){
          LatticeColorMatrix tempL;
          tempL=0;
          tempL[color[0]]=tempu[m]*shift(tempu[n],FORWARD,m)*shift(adj(tempu[m]),FORWARD,n)*adj(tempu[n])+tempu[m]*shift(shift(adj(tempu[n]),BACKWARD,n),FORWARD,m)*shift(adj(tempu[m]),BACKWARD,n)*shift(tempu[n],BACKWARD,n);
          Double temp=sum(real(trace(tempL)));
          S_pg+=temp;
        }
      }
    }
  }

  if(l==0) S_pg /= Double(4.0);

  // Normalize
  S_pg *= Double(-1.0)/Double(Nc);
  END_CODE();

  return S_pg;
}




//------------------------------------------------------------------------

  void recursiveMG(XMLWriter& xml_out, multi1d<LatticeColorMatrix>& u,
	    MGControl& mg_control, 
            Set color)//,           ActionParam& act)  
  {

    START_CODE();
    // numSubsets()=4*((1<<l)-1)+1
    // Calculate l from color's numsubsets and a little loop
    int t=(color.numSubsets()-1)/4+1;// t starts as 2^l
    int l=0;
    while(t >= 2){t=t >> 1; l++;}
    
    QDPIO::cout<<l<<std::endl;

    TheNamedObjMap::Instance().getData<Int>("LEVEL")=Int(l);


    Handle< LinearGaugeAction > S_g;
    // This will remember the action for after the mgupdate
    QDPIO::cout<<"Making action"<<'\n';
    try
    {
      std::istringstream is(mg_control.hbitr_params.hb_gaugeact.gauge_act);
      XMLReader gaugeact_reader(is);

      // Get the name of the gauge act
      std::string gaugeact_string;
      std::string filename;
      try
      { 
        read(gaugeact_reader, "/GaugeAction/Name", gaugeact_string);
      }
      catch( const std::string& e) 
      {
        QDPIO::cerr << "Error grepping the gaugeact name: " << e<<  std::endl;
        QDP_abort(1);
      }

      // Throw an exception if not found
      typedef multi1d<LatticeColorMatrix>  P;
      typedef multi1d<LatticeColorMatrix>  Q;

     GaugeAction<P,Q>* gaugeact = 
             TheGaugeActFactory::Instance().createObject(gaugeact_string, 
						  gaugeact_reader, 
						  "/GaugeAction");
      S_g = dynamic_cast<LinearGaugeAction*>(gaugeact);
    }
    catch(std::bad_cast) 
    {
      QDPIO::cerr << "PURGAUG: caught cast error" << std::endl;
      QDP_abort(1);
    }
    catch(std::bad_alloc) 
    { 
      // This might happen on any node, so report it
      std::cerr << "PURGAUG: caught bad memory allocation" << std::endl;
      QDP_abort(1);
    }
    catch(const std::string& e) 
    {
      QDPIO::cerr << "PURGAUG: Caught Exception: " << e << std::endl;
      QDP_abort(1);
    } 
    catch(std::exception& e) 
    {
      QDPIO::cerr << "PURGAUG: Caught standard library exception: " << e.what() <<  std::endl;
      QDP_abort(1);
    }
    catch(...)
    {
      // This might happen on any node, so report it
      std::cerr << "PURGAUG: caught generic exception during measurement" << std::endl;
      QDP_abort(1);
    }

    //Handle< GaugeState<P,Q> > state(S_g.createState(u));
    QDPIO::cout<<"preupdate"<<l<<std::endl;
    for(int i=0; i<mg_control.hbitr_params.npre; i++){ 
      miter(u, *S_g, mg_control.hbitr_params.hb_params, color); 
    } //npre hb sweeps



    if(l < mg_control.hbitr_params.lmax){
    
      multi1d<LatticeColorMatrix> delta(Nd);
      multi1d<LatticeColorMatrix> g(Nd);
      // calculate coefficients and coarsen lattice
      multi1d<LatticeColorMatrix> oldu;
      oldu=u;

      QDPIO::cout<<"coarsen"<<l<<std::endl;
      color.make(ColorFunc(l+1));
      coarsen(u, delta, g, color);


      // This method may not work properly for gam>1, but this can be worked around later
      actcoeff(oldu, delta, color);
      


      multi2d<LatticeColorMatrix> olda(Nd,Nd);
      multi2d<LatticeColorMatrix> oldb(Nd,Nd);
      multi2d<LatticeColorMatrix> oldc(Nd,Nd);
      multi2d<LatticeColorMatrix> oldd(Nd,Nd);
      multi1d<LatticeColorMatrix> oldh(Nd);
      olda=TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> >("Actiona");
      oldb=TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> >("Actionb");
      oldc=TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> >("Actionc");
      oldd=TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> >("Actiond");
      oldh=TheNamedObjMap::Instance().getData< multi1d<LatticeColorMatrix> >("Actionh");
  


      // call recursive update (gam=1 is V cycle, gam=2 is W cycle)
      for(int i=0; i<mg_control.hbitr_params.gam; i++)
      {
        recursiveMG(xml_out, u, mg_control, color);

        // Resets for the action for the W cycle
        TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> >("Actiona")= olda;
        TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> >("Actionb")= oldb;
        TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> >("Actionc")= oldc;
        TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> >("Actiond")= oldd;
        TheNamedObjMap::Instance().getData< multi1d<LatticeColorMatrix> >("Actionh")= oldh;
      }


      // refine the lattice from the updated coarse lattice
      finen(u,oldu,color,delta,g);
      color.make(ColorFunc(l));

    }


    QDPIO::cout<<"post update"<<l<<std::endl;
    for(int i=0; i<mg_control.hbitr_params.npost; i++){ 
      miter(u, *S_g, mg_control.hbitr_params.hb_params, color); 
    } //npost hb sweeps
    END_CODE();

  }


//------------------------------------------------------------------------


  void doWarmUp(XMLWriter& xml_out,
		multi1d<LatticeColorMatrix>& u,
		MGControl& mg_control,
		const multi1d< Handle< AbsInlineMeasurement > >& default_measurements,
		const multi1d< Handle<AbsInlineMeasurement> >& user_measurements) //,           ActionParam& act)  
  {
    START_CODE();

    // Set the update number
    unsigned long cur_update = 0;
      
    // Compute how many updates to do
    unsigned long to_do = mg_control.mc_control.n_warm_up_updates;
      
    QDPIO::cout << "WarmUp Control: About to do " << to_do << " updates" << std::endl;

    // XML Output
    push(xml_out, "WarmUpdates");

    for(int i=0; i < to_do; i++) 
    {
      push(xml_out, "elem"); // Caller writes elem rule

      push(xml_out, "Update");
      // Increase current update counter
      cur_update++;
	
      // Log
      write(xml_out, "update_no", cur_update);
      write(xml_out, "WarmUpP", true);

      Set color;
      color.make(ColorFunc(0));

      // Do the update, but with no measurements
      recursiveMG(xml_out, u, mg_control, color);//,act); //one mg sweep
   
      // Do measurements
      doMeas(xml_out, u, mg_control, true, cur_update,
	     default_measurements, user_measurements);

      // shift the lattice so the same links aren't being updated everytime
      multi1d<LatticeColorMatrix> temp(Nd); temp = u;
      int dir; srand(time(NULL)); dir = rand()%4;
      for(int m=0;m<Nd;m++) u[m] = shift(temp[m],FORWARD,dir);

      pop(xml_out); // pop("Update");
      pop(xml_out); // pop("elem");
    }

    pop(xml_out); // pop("WarmUpdates")
    
    END_CODE();
  }
  

  //--------------------------------------------------------------------------


  void doProd(XMLWriter& xml_out,
	      multi1d<LatticeColorMatrix>& u,
	      MGControl& mg_control, 
	      const multi1d< Handle< AbsInlineMeasurement > >& default_measurements,
	      const multi1d< Handle<AbsInlineMeasurement> >& user_measurements) //,           ActionParam& act)  
  {
    START_CODE();
    // Set the update number
    unsigned long cur_update = mg_control.mc_control.start_update_num;
      
    // Compute how many updates to do
    unsigned long total_updates = mg_control.mc_control.n_production_updates;
      
    unsigned long to_do = 0;
    if ( total_updates > mg_control.mc_control.n_updates_this_run + cur_update +1 ) {
      to_do = mg_control.mc_control.n_updates_this_run;
    }
    else {
      to_do = total_updates - cur_update ;
    }
      
    QDPIO::cout << "MC Control: About to do " << to_do << " updates" << std::endl;

    // XML Output
    push(xml_out, "MCUpdates");

    for(int i=0; i < to_do; i++) 
    {
      push(xml_out, "elem"); // Caller writes elem rule

      push(xml_out, "Update");
      // Increase current update counter
      cur_update++;
	
      // Decide if the next update is a warm up or not
      QDPIO::cout << "Doing Update: " << cur_update << " warm_up_p = " << false << std::endl;

      // Log
      write(xml_out, "update_no", cur_update);
      write(xml_out, "WarmUpP", false);

      Set color;
      color.make(ColorFunc(0));

      // Do the update
      recursiveMG(xml_out, u, mg_control, color);//,act); // one mg sweep
      //mciter(u, S_g, mg_control.hbitr_params.hb_params); //one hb sweep

      // Do measurements
      doMeas(xml_out, u, mg_control, false, cur_update,
	     default_measurements, user_measurements);

      // shift the lattice so the same links aren't being updated everytime
      multi1d<LatticeColorMatrix> temp(Nd); temp = u;
      int dir; srand(time(NULL)); dir = rand()%4;
      for(int m=0;m<Nd;m++) u[m] = shift(temp[m],FORWARD,dir);

      // Save if needed
      if( cur_update % mg_control.mc_control.save_interval == 0 ) 
      {
        for(int m=0;m<Nd;m++) reunit(u[m]);
	saveState(mg_control.hbitr_params, mg_control.mc_control, 
		  cur_update,
		  mg_control.inline_measurement_xml, u);
      }

      pop(xml_out); // pop("Update");
      pop(xml_out); // pop("elem");
    }

    pop(xml_out); // pop("MCUpdates")
    
    END_CODE();
  }


  //--------------------------------------------------------------------------


  void doMG(multi1d<LatticeColorMatrix>& u,
	    MGControl& mg_control, 
	    multi1d< Handle<AbsInlineMeasurement> >& user_measurements) 
  {
    START_CODE();
    QDPIO::cout<<"doMG"<<'\n';

    XMLWriter& xml_out = TheXMLOutputWriter::Instance();
    push(xml_out, "doMG");

    multi1d< Handle< AbsInlineMeasurement > > default_measurements(1);
    InlinePlaquetteEnv::Params plaq_params;
    plaq_params.frequency = 1;
    // It is a handle
    default_measurements[0] = new InlinePlaquetteEnv::InlineMeas(plaq_params);


    // Initialize the action and create a named object
    try
    {
      TheNamedObjMap::Instance().create<Int>("LEVEL");
      TheNamedObjMap::Instance().create<multi2d<LatticeColorMatrix>>("Actiona");
      TheNamedObjMap::Instance().create<multi2d<LatticeColorMatrix>>("Actionb");
      TheNamedObjMap::Instance().create<multi2d<LatticeColorMatrix>>("Actionc");
      TheNamedObjMap::Instance().create<multi2d<LatticeColorMatrix>>("Actiond");
      TheNamedObjMap::Instance().create<multi1d<LatticeColorMatrix>>("Actionh");
    }
    catch (std::bad_cast)
    {
      QDPIO::cerr <<"doMG" <<": caught dynamic cast error" 
		  << std::endl;
      QDP_abort(1);
    }
    catch (const std::string& e) 
    {
      QDPIO::cerr << "doMG" << ": error creating action: " << e << std::endl;
      QDP_abort(1);
    }

    // Sets first int, but is also done in multigrid.
    TheNamedObjMap::Instance().getData<Int>("LEVEL")=Int(0);

    Set color;
    color.make(ColorFunc(0));
    multi1d<LatticeColorMatrix> dud(Nd);
    multi1d<LatticeColorMatrix> dud2(Nd);
    actcoeff(dud2,dud,color); // This initializes the named object matrices

    try 
    {
      // Initialise the RNG
      QDP::RNG::setrn(mg_control.mc_control.rng_seed);
      
      // If warmups are required, do them first
      if (mg_control.mc_control.n_warm_up_updates > 0)
      {
	doWarmUp(xml_out, u, mg_control, default_measurements, user_measurements); //,act);
	mg_control.mc_control.n_warm_up_updates = 0;  // reset
      }

      // Do the production updates
      doProd(xml_out, u, mg_control, default_measurements, user_measurements); //,act );
    }
    catch( const std::string& e) { 
      QDPIO::cerr << "Caught Exception: " << e << std::endl;
      QDP_abort(1);
    }

    pop(xml_out);
    
    END_CODE();
  }
  
  bool linkageHack(void)
  {
    bool foo = true;
    
    // Gauge actions
    foo &= GaugeActsEnv::registerAll();

    // Inline Measurements
    foo &= InlineAggregateEnv::registerAll();

    return foo;
  }

};


  //--------------------------------------------------------------------------


using namespace Chroma;

//! Pure gauge field generation via heatbath
/*! \defgroup purgaug Heat-bath
 *  \ingroup main
 *
 * Main program for heat-bath generation of 
 */

int main(int argc, char *argv[]) 
{
  Chroma::initialize(&argc, &argv);
  
  START_CODE();
  // Chroma Init stuff -- Open DATA and XMLDAT
  linkageHack();
  XMLFileWriter& xml_out = Chroma::getXMLOutputInstance();
  push(xml_out, "purgaug");
  MGControl  mg_control;
  try
  {
    XMLReader xml_in(Chroma::getXMLInputFileName());

    read(xml_in, "/purgaug", mg_control);

    // Write out the input
    write(xml_out, "Input", xml_in);
  }
  catch( const std::string& e ) {
    QDPIO::cerr << "Caught Exception reading input XML: " << e << std::endl;
    QDP_abort(1);
  }
  catch( std::exception& e ) {
    QDPIO::cerr << "Caught standard library exception: " << e.what() << std::endl;
    QDP_abort(1);
  }
  catch(...) {
    QDPIO::cerr << "Caught unknown exception " << std::endl;
    QDP_abort(1);
  }
  Layout::setLattSize(mg_control.hbitr_params.nrow);
  Layout::create();

  proginfo(xml_out);    // Print out basic program info

  // Start up the config
  multi1d<LatticeColorMatrix> u(Nd);
  {
    XMLReader file_xml;
    XMLReader config_xml;
    
    gaugeStartup(file_xml, config_xml, u, mg_control.cfg);

    // Write out the config header
    push(xml_out, "Config_info");
    write(xml_out, "file_xml", file_xml);
    write(xml_out, "config_xml", config_xml);
    pop(xml_out);
  }


  // The create gauge action code was pushed till during the MG update
  // Create the gauge action
  // This code is limited to only rb sets and subsets.
  // The main point is
  // The number of subsets within the staples, etc. and to get the subsets
  // straight
  /*
  Handle< LinearGaugeAction > S_g;
  try
  {
    std::istringstream is(mg_control.hbitr_params.hb_gaugeact.gauge_act);
    XMLReader gaugeact_reader(is);

    // Get the name of the gauge act
    std::string gaugeact_string;
    try { 
      read(gaugeact_reader, "/GaugeAction/Name", gaugeact_string);
    }
    catch( const std::string& e) 
    {
      QDPIO::cerr << "Error grepping the gaugeact name: " << e<<  std::endl;
      QDP_abort(1);
    }

    // Throw an exception if not found
    typedef multi1d<LatticeColorMatrix>  P;
    typedef multi1d<LatticeColorMatrix>  Q;

    GaugeAction<P,Q>* gaugeact = 
      TheGaugeActFactory::Instance().createObject(gaugeact_string, 
						  gaugeact_reader, 
						  "/GaugeAction");
    S_g = dynamic_cast<LinearGaugeAction*>(gaugeact);
  }
  catch(std::bad_cast) 
  {
    QDPIO::cerr << "PURGAUG: caught cast error" << std::endl;
    QDP_abort(1);
  }
  catch(std::bad_alloc) 
  { 
    // This might happen on any node, so report it
    std::cerr << "PURGAUG: caught bad memory allocation" << std::endl;
    QDP_abort(1);
  }
  catch(const std::string& e) 
  {
    QDPIO::cerr << "PURGAUG: Caught Exception: " << e << std::endl;
    QDP_abort(1);
  }
  catch(std::exception& e) 
  {
    QDPIO::cerr << "PURGAUG: Caught standard library exception: " << e.what() << std::endl;
    QDP_abort(1);
  }
  catch(...)
  {
    // This might happen on any node, so report it
    std::cerr << "PURGAUG: caught generic exception during measurement" << std::endl;
    QDP_abort(1);
  }
  */

  // Get the measurements
  multi1d < Handle< AbsInlineMeasurement > > the_measurements;

  try { 
    std::istringstream Measurements_is(mg_control.inline_measurement_xml);

    XMLReader MeasXML(Measurements_is);

    std::ostringstream os;
    MeasXML.print(os);
    QDPIO::cout << os.str() << std::endl << std::flush;

    read(MeasXML, "/InlineMeasurements", the_measurements);
  }
  catch(const std::string& e) { 
    QDPIO::cerr << "Caugth exception while reading measurements: " << e << std::endl
		<< std::flush;

    QDP_abort(1);
  }

  QDPIO::cout << "There are " << the_measurements.size() << " user measurements " << std::endl;

  
  // Run
  try 
  {
    doMG(u, mg_control, the_measurements);
    //doHB(u, *S_g, mg_control, the_measurements);
  } 
  catch(std::bad_cast) 
  {
    QDPIO::cerr << "PURGAUG: caught cast error" << std::endl;
    QDP_abort(1);
  }
  catch(std::bad_alloc) 
  { 
    QDPIO::cerr << "PURGAUG: caught bad memory allocation" << std::endl;
    QDP_abort(1);
  }
  catch(const std::string& e) 
  { 
    QDPIO::cerr << "PURGAUG: Caught std::string exception: " << e << std::endl;
    QDP_abort(1);
  }
  catch(std::exception& e) 
  {
    QDPIO::cerr << "PURGAUG: Caught standard library exception: " << e.what() << std::endl;
    QDP_abort(1);
  }
  catch(...) 
  {
    QDPIO::cerr << "PURGAUG: Caught generic/unknown exception" << std::endl;
    QDP_abort(1);
  }

  pop(xml_out);

  END_CODE();

  Chroma::finalize();
  exit(0);
}

