// $Id: t_skeleton.cc,v 1.5 2003-06-07 19:09:32 edwards Exp $
/*! \file
 *  \brief Skeleton of a QDP main program
 */

#include "chroma.h"
#include <qdp-lapack.h>
#include <iostream>
using namespace Chroma;

Int sign(Real t)
{
  return Int(toBool(Real(0) < t) - toBool(Real(0) > t));
}

LatticeInt sign(LatticeReal t)
{
  return LatticeInt(Real(0) < t) - LatticeInt(Real(0) > t);
}

DComplex power(DComplex z, Real n)
{
  Real r(pow(real(z)*real(z)+imag(z)*imag(z),Real(.5)));
  Real phi(acos(real(z)/r));
  Int sgn(sign(imag(z)));
  Real temp=phi;
  phi=temp + (sgn-Int(1))*(temp-Real(3.141592));// In the case of phi=0,pi, this will always map to pi
  return cmplx(pow(r,n)*cos(phi*n),pow(r,n)*sin(phi*n));
}

LatticeComplex power(LatticeComplex z, Real n)
{
  LatticeReal r(pow(real(z)*real(z)+imag(z)*imag(z),Real(.5)));
  // This phi is between 0 and pi, it is the same regardless of the sign of imag(z)
  LatticeReal phi(acos(real(z)/r));
  LatticeInt sgn(sign(imag(z)));
  LatticeReal temp=phi;
  // This sets phi to be the counterclockwise angle from the x axis
  phi=temp + (sgn-Int(1))*(temp-Real(3.141592));// In the case of asin(imag(z)=0 (phi=0,PI), this will always map to PI
  return cmplx(pow(r,n)*cos(phi*n),pow(r,n)*sin(phi*n));
}

class Function
{
public:
  Real n;
  Real s;
  Function(Real a) : n(a), s(Real(0)) {}
  virtual LatticeComplex operator () (const Real t) =0;
  void setS(Real t){s=t;}
  void setN(Real a){n=a;}
};

// Evaluates the integral dl/(2pi i) l^(n/2)/((l-l1)(l-l2)(l-l3))
LatticeComplex integral(Real a, Real b, int steps, Function& f)
{
  Real ds((b-a)/Real(steps));
  LatticeComplex res;
  // The first and last term in sum will contribute to only one trapezoid.
  //The first term is of an integral from a to a
  res = cmplx(Real(0),Real(0));  

  if(toBool(a!=b))
  {
    res = f(a);
    res += Real(4)*f(a+ds/Real(2));
    for(Real s(a+ds); toBool(s < b); s+=ds)
    {
      res += Real(2)*f(s);
      res += Real(4)*f(s+ds/Real(2));
    }
    res += f(b);
  
    // multiply by width of the s trapezoids
    res = res*ds/Real(6);
  }
  return res;
}

class Inte: public Function
{
public:
  Inte(Real a) : Function(a) {}

  LatticeComplex operator() (const Real a){ LatticeComplex res; res= cmplx(a,Real(0)); return res; }
  LatticeComplex operator() (const multi1d<LatticeComplex> l)
  {
    LatticeComplex res; res=(cmplx(Real(0),Real(0)));
    for(int i=0;i<3;i++){
      LatticeComplex temp=power(l[i],n);
      for(int j=0;j<3;j++){
        if(j!=i) temp/=(l[i]-l[j]);
      }
      res+=temp;
    }
    return res;
  }
  ~Inte(){}
};

class Testfunc: public Function
{
public:
  Testfunc(Real a) : Function(a) {}
  
  LatticeComplex operator() (const Real t)
  {
    LatticeComplex res;
    Complex z,w;
    z = cmplx(Real(-.5), Real(pow(.75,.5)));
    w = cmplx(Real(pow(.5,.5)), Real(pow(.5,.5)));
    res = power(w*s+t*z,n);
    return res;
  }
};

class Halfn : public Function
{
public:
  multi1d<LatticeComplex> l;
  Halfn(Real a,multi1d<LatticeComplex> b) : Function(a), l(b) {}
  LatticeComplex operator() (const Real t)
  {
    return n*(n-Real(1))*power((1-s)*l[0] + (s-t)*l[1] + t*l[2],n-Real(2));
  }
};

class Tint : public Function
{
public:
  int steps;
  Function& f;
  Tint(Real a, int st, Function& g) :Function(a), steps(st),f(g) {}

  LatticeComplex operator() (const Real t)
  {
    f.setS(t);
    return integral(Real(0),t,steps,f);
  }
  void setN(Real a){
    n = a;
    f.setN(a);
  }
};


void findEvalues(LatticeColorMatrix& mat, multi1d<LatticeComplex>& l)
{
  // P(x) = x^3 + a x^2 + b x - 1
  LatticeComplex a,b,d;
  a = -trace(mat);
  b = -conj(a);// for special unitary mat  0.5*(trace(mat)*trace(mat)-trace(mat*mat))=conj(trace(mat))

  multi1d<Complex> z(3);
  z[0]=cmplx(Real(1),Real(0));
  z[1]=cmplx(Real(-.5),Real(pow(.75,.5)));
  z[2]=cmplx(Real(-.5),Real(-pow(.75,.5)));

  LatticeComplex d1, d0;
  d1 = Real(2)*a*a*a - Real(9)*a*b - Real(27);
  d0 = a*a - Real(3)*b;
  d = power((d1 + power(d1*d1-Real(4)*d0*d0*d0,Real(.5)))/Real(2), Real(1.0/3.0));
  
  //e-values
  for(int i=0; i<3; i++)
  {
    l[i] = -(a + z[i]*d + conj(z[i])*d0/d)/Real(3);
  }
}

int main(int argc, char *argv[])
{
  // Put the machine into a known state
  Chroma::initialize(&argc, &argv);
  
  START_CODE();
  // Setup the lattice size
  // NOTE: in general, the user will need/want to set the
  // lattice size at run-time
  multi1d<int> nrow(Nd);
  for(int i=0; i < Nd; ++i)
    nrow[i] = 2;         // Set the lattice size to 2^4

  // Insert the lattice size into the Layout
  // There can be additional calls here like setting the number of
  // processors to use in an SMP implementation
  Layout::setLattSize(nrow);

  // Create the lattice layout
  // Afterwards, QDP is useable
  Layout::create();
  // Do some wonderful and amazing things - impress your friends
  multi1d<LatticeColorMatrix> u;
  u.resize(Nd);
  HotSt(u);
  multi1d<LatticeColorMatrix> sqrtu(Nd);
  multi1d<LatticeComplex> l(Nc);
  LatticeComplex a=-trace(u[0]);
  findEvalues(u[0],l);

  // Declare a function
  //int steps=100;
  //Testfunc testee(n);
  Halfn testee(Real(.5),l);
  
   Inte in(Real(.5));
  LatticeComplex exact = in(l);
  multi1d<int> origin(Nd); for(int i=0;i<Nd;i++) origin[i]=0;
  multi1d<int> two(Nd); two=origin;
  two[0]=1; two[1]=1;
  QDPIO::cout<< "Exact" << '\t' << real(peekSite(exact,origin))<<'\t'<<imag(peekSite(exact,origin))<<'\t' << real(peekSite(exact,two))<<'\t'<<imag(peekSite(exact,two)) << '\n' << '\n';
  for(int steps=20; steps<=300; steps += 20)
  {
    Tint test(Real(.5),steps,testee);
  
    LatticeComplex tester1;
    test.setN(Real(.5));
    tester1 = integral(Real(0),Real(1),steps,test);
    QDPIO::cout<< Int(steps) << '\t' << real(peekSite(tester1,origin))<<'\t'<<imag(peekSite(tester1,origin))<<'\t' << real(peekSite(tester1,two))<<'\t'<<imag(peekSite(tester1,two)) << '\n';
  }

  int steps=500;
  Tint test(Real(.5),steps,testee);

  LatticeComplex tester1;
  test.setN(Real(.5));
  tester1 = integral(Real(0),Real(1),steps,test);

  LatticeComplex tester2;
  test.setN(Real(1.5));
  tester2 = integral(Real(0),Real(1),steps,test);

  LatticeComplex tester3;
  test.setN(Real(2.5));
  tester3 = integral(Real(0),Real(1),steps,test);
  
  /*multi1d<LatticeComplex> z(3);
  z[0]=cmplx(Real(1),Real(0));
  z[1]=cmplx(Real(-.5),Real(pow(.75,.5)));
  z[2]=cmplx(Real(-.5),Real(-pow(.75,.5)));*/

  XMLFileWriter xml_out("t1.out.xml");
  push(xml_out,"Test");
  write(xml_out,"t1", tester1);
  pop(xml_out);
  xml_out.close();  

  xml_out.open("t2.out.xml");
  push(xml_out,"Test");
  write(xml_out,"t2", tester2);
  pop(xml_out);
  xml_out.close();  

  xml_out.open("t3.out.xml");
  push(xml_out,"Test");
  write(xml_out,"t3", tester3);
  pop(xml_out);
  xml_out.close();
  
  xml_out.open("difft1.out.xml");
  push(xml_out,"Test");
  write(xml_out,"t1", tester1-in(l));
  pop(xml_out);
  xml_out.close();

  in.setN(Real(1.5));
  xml_out.open("difft2.out.xml");
  push(xml_out,"Test");
  write(xml_out,"t2", tester2-in(l));
  pop(xml_out);
  xml_out.close();  

  in.setN(Real(2.5));  
  xml_out.open("difft3.out.xml");
  push(xml_out,"Test");
  write(xml_out,"t3", tester3-in(l));
  pop(xml_out);
  xml_out.close();
  
  LatticeComplex f0,f1,f2;

  /* test.setN(Real(.5));
  f2=integral(Real(0),Real(1),steps,test);
  std::cout<<"Found f2 "<< '\n';
  test.setN(Real(1.5));
  f1=a*f2 + integral(Real(0),Real(1),steps,test); 
  std::cout<<"Found f1 "<< '\n';
  test.setN(Real(2.5));
  f0=(-conj(a)-a*a)*f2 + a*f1 + integral(Real(0),Real(1),steps,test);  std::cout<<"Found f0 "<< '\n';
  
  sqrtu = f0 + f1*u[0] + f2*u[0]*u[0];
  
     
  xml_out.open("f1.out.xml");
  push(xml_out,"Test");
  write(xml_out,"f1", f1);
  pop(xml_out);
  xml_out.close();

  xml_out.open("f2.out.xml");
  push(xml_out,"Test");
  write(xml_out,"f2", f2);
  pop(xml_out);
  xml_out.close();

  xml_out.open("f0.out.xml");
  push(xml_out,"Test");
  write(xml_out,"f0", f0);
  pop(xml_out);
  xml_out.close();*/

  /*
  xml_out.open("trace.out.xml");
  push(xml_out,"Test");
  write(xml_out,"matrix", trace(u[0]));
  pop(xml_out);
  xml_out.close();*/

  xml_out.open("sum.out.xml");
  push(xml_out,"Test");
  write(xml_out,"matrix", l[0]+l[1]+l[2]);
  pop(xml_out);
  xml_out.close();

  xml_out.open("det.out.xml");
  push(xml_out,"Test");
  write(xml_out,"matrix", l[0]*l[1]*l[2]);
  pop(xml_out);
  xml_out.close();

  xml_out.open("l1.out.xml");
  push(xml_out,"Test");
  write(xml_out,"l1", l[0]);
  pop(xml_out);
  xml_out.close();

  xml_out.open("l2.out.xml");
  push(xml_out,"Test");
  write(xml_out,"l2", l[1]);
  pop(xml_out);
  xml_out.close();

  xml_out.open("l3.out.xml");
  push(xml_out,"Test");
  write(xml_out,"l3", l[2]);
  pop(xml_out);
  xml_out.close();

  xml_out.open("mat.out.xml");
  push(xml_out,"Test");
  write(xml_out,"matrix", u[0]);
  pop(xml_out);
  xml_out.close();
    
  xml_out.open("sqrt.out.xml");
  push(xml_out,"Test");
  write(xml_out,"sqrt", sqrtu[0]);
  pop(xml_out);
  xml_out.close();

  xml_out.open("diff.out.xml");
  push(xml_out,"Test");
  write(xml_out,"sqrt", sqrtu[0]*sqrtu[0]-u[0]);
  pop(xml_out);
  xml_out.close();
  
  // Possibly shutdown the machine
  END_CODE();

  Chroma::finalize();
  exit(0);
}
