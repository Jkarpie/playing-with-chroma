// $Id: t_skeleton.cc,v 1.5 2003-06-07 19:09:32 edwards Exp $
/*! \file
 *  \brief Skeleton of a QDP main program
 */

#include "chroma.h"
#include <iostream>
#include <qdp-lapack.h>
using namespace Chroma;

// A coloring function to find the active sites on the coarse lattice (when color = 0).
class ColorFunc : public SetFunc{
private:
  int l ;
public:
  ColorFunc(int l_):l(l_)  {}
  
  int operator()(const multi1d<int>& coord) const {
    int a=0;
    for(int mu=0; mu < Nd; mu++){a+=coord[mu]%(1<<l);}
    return a;
  }
  int numSubsets() const {
    return 4*((1<<l)-1)+1;// for l=1, the possible values are 0, 1, 2, 3, 4
  }  
} ;



  // returns the sign of t
  Int sign(Real t)
  {
    Int ans = Int(Real(0) < t) - Int(Real(0) > t);

    // In the case of phi=0/pi, ans= 0 not +1/-1, this will always map to phi to pi,
    // This if statement sets phi to 0 instead.
    if(toBool(ans!=Int(0))) return ans;
    else return Int(1);
  }


  //--------------------------------------------------------------------------


  // returns z^n For fractional n, returns z^n = r^n e^(i n phi) where phi is [0,2pi]
  Complex power(Complex z, Real n)
  {
    START_CODE();
    Real r(pow(real(z)*real(z)+imag(z)*imag(z),Real(.5)));
    Real phi(acos(real(z)/r));
    Int sgn(sign(imag(z)));
    Real temp=phi;
    phi=temp + (sgn-Int(1))*(temp-Real(3.141592));
    END_CODE();

    return cmplx(pow(r,n)*cos(phi*n),pow(r,n)*sin(phi*n));
  }


  //--------------------------------------------------------------------------

  // Finds the SU(3) matrices which are the sqrt of u
  LatticeColorMatrix sqrt(LatticeColorMatrix& u, Set color)
  {
    START_CODE();
    int t=(color.numSubsets()-1)/4+1;// t starts as 2^l
    int l=0;
    while(t >= 2){t=t>>1; l++;}

    LatticeColorMatrix sq=1;
    multi1d<int> x(Nd);
    for(int i=0;i<Nd;i++) x[i]=0;
    multi1d<int> nrow(Nd);

    nrow = Layout::lattSize();
    int stepsize=1<<l;

    while(x[0] < nrow[0]){
      while(x[1] < nrow[1]){
        while(x[2] < nrow[2]){
          while(x[3] < nrow[3]){ 
            ColorMatrix sqrtu;
  
            multi2d<DComplex> mat;
            mat.resize(Nc,Nc);
  
            ColorMatrix v = peekSite(u,x);
            ColorVector halfevtemp;
            ColorVector temp;

	    // Fill the matrix for lapack
            for(int i=0; i<Nc; i++)
            {
              for(int j=0; j<Nc; j++)
              {
                mat[i][j]=peekColor(v,i,j);
              }
            }

            multi1d<DComplex> values;
            values.resize(Nc);
            multi2d<DComplex> vectors;
            vectors.resize(Nc,Nc);

            // Find evalues and evectors
            QDPLapack::zgeev(Nc, mat, values, vectors);

            // Fill CMs with evalue^.5 diagonal and evector columns
	    ColorMatrix l=1;
            ColorMatrix s=1;

  	    Complex det(cmplx(Real(1),Real(0)));
            for(int i=0; i<Nc; i++)
            {

              temp = pokeColor(temp,(Complex) values[i], i);

              for(int j=0; j<Nc; j++)
              {
                //l = pokeColor(l, cmplx(Real(0),Real(0)) ,i,j); //if l is set to 1 then all off diag are 0
                Complex vec((Complex) vectors[i][j]);
                s = pokeColor(s, vec,i,j);
              }
              Complex eval(power((Complex) values[i],Real(.5)));
	      det *= eval;
              l = pokeColor(l, (Complex) eval, i, i);
            }
            sqrtu = adj(s)*l*s;
	  
	    if(toBool(real(det)<Real(0))) sqrtu*=-1;

  	    sq = pokeSite(sq,sqrtu,x);  
            x[3] += stepsize;
          }
	  x[3] = 0;
          x[2] += stepsize;
        }
        x[2] = 0;
        x[1] += stepsize;
      }
      x[1] = 0;
      x[0] += stepsize;
    }

    END_CODE();
    return sq;
  }


  //--------------------------------------------------------------------------


  // Returns LCM a shifted by dist steps
  // The use of this method as written is inefficient for higher l, in most cases dist = 2^l
  LatticeColorMatrix shifter(const LatticeColorMatrix& a,
                             int sign, int dir, int dist){
    START_CODE();
    LatticeColorMatrix b;
    LatticeColorMatrix c;
    b=a;
    for(int i=0;i<dist;++i){
      c=shift(b,sign,dir);
      b=c;
    }
    return b;
    END_CODE();
  }


  //--------------------------------------------------------------------------


  // A method to coarsen u on level l-1 to level l
  void coarsen(multi1d<LatticeColorMatrix>& u,
               multi1d<LatticeColorMatrix>& delta, 
               multi1d<LatticeColorMatrix>& g, 
               Set color)
  {
    START_CODE();
 
    int t=(color.numSubsets()-1)/4+1;// t starts as 2^l
    int l=0;
    while(t >= 2){t=t>>1; l++;}

  
    for(int mu(0); mu<Nd; mu++)
    {  // Shifts links such that u[color[0]] and u2[color[0]] are the two to be combined.
      LatticeColorMatrix u2=1;
      LatticeColorMatrix temp=1;
      u2[color[0]]=shifter(u[mu],FORWARD,mu,(1<<(l-1)));
      delta[mu]=1;
      g[mu]=1;

 
      // Set the gauge transformation by shifting the temp
      temp[color[0]]= u[mu]*u2;

 /* std::ostringstream os15;
  os15<<"sqrtv"<<"level"<<l<<"dir"<<mu<<".out.xml";

  XMLFileWriter xml_out15(os15.str());
  push(xml_out15,"LEVEL");
  write(xml_out15,"v",sqrt(temp,color));
  pop(xml_out15);
  xml_out15.close();*/


      g[mu][color[0]]=adj(sqrt(temp,color))*u[mu];
      temp=g[mu];
      g[mu]=shift(temp,BACKWARD,mu);


  /*std::ostringstream os;
  os<<"gauge"<<"level"<<l<<"dir"<<mu<<".out.xml";

  XMLFileWriter xml_out2(os.str());
  push(xml_out2,"LEVEL");
  write(xml_out2,"gauge",g[mu]);
  pop(xml_out2);
  xml_out2.close();*/

      // Do the gauge transformation
      for(int nu(0); nu < Nd; nu++)
      {
        temp=u[nu];
        u[nu]=g[mu]*temp*adj(shift(g[mu],FORWARD,nu));
      }
      delta[mu][color[0]]=u[mu]*adj(u2);
    }
    END_CODE();
  }

  //--------------------------------------------------------------------------

  void finen(multi1d<LatticeColorMatrix>& u,
             multi1d<LatticeColorMatrix>& oldu,
             Set color,
             multi1d<LatticeColorMatrix>& delta,
             multi1d<LatticeColorMatrix>& g)
  {
    START_CODE();
    int t=(color.numSubsets()-1)/4+1;// t starts as 2^l
    int l=0;
    while(t >= 2){t=t>>1; l++;}

    // This loop will undo the gauge trasformation that was applied during coarsening, then set u to new values using delta
    for(int m=0; m<Nd; m++){
      LatticeColorMatrix v=1;
      LatticeColorMatrix u2=1;
      LatticeColorMatrix temp=1;

      // Hold this for after gauge transformation is undone.
      v[color[0]] = u[m];
 
      // old u2 is usually stored on the lattice, except for the case of l=1
      u2[color[0]]=shifter(oldu[m],FORWARD,m,(1<<(l-1)));
  
      // Undo the gauge transformation
      for(int nu(0); nu < Nd; nu++)
      {
        temp = u[nu];
        u[nu] = adj(g[m])*temp*shift(g[m],FORWARD,nu);
      }
           
      // Apply the fine/coarse link relation
      u[m][color[0]] = v*delta[m];
      temp = shifter(u[m],FORWARD,m,(1<<(l-1)));
      temp[color[0]] = adj(delta[m])*v;
      u[m] = shifter(temp,BACKWARD,m,1<<(l-1));

    }
    END_CODE();
  }



  //--------------------------------------------------------------------------


  // Calculated the coefficient matrices for the action
  void actcoeff(multi1d<LatticeColorMatrix>& u,
                multi1d<LatticeColorMatrix>& delta, 
                Set color) //,           ActionParam& act)  
  {
    START_CODE();
    int t=(color.numSubsets()-1)/4+1;// t starts as 2^l
    int l=0;
    while(t >= 2){t=t>>1; l++;}

    // Do I need to manipulating the xml files here?
    try
    {
      TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> > ("Actiona");
      TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> > ("Actionb");
      TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> > ("Actionc");
      TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> > ("Actiond");
      TheNamedObjMap::Instance().getData< multi1d<LatticeColorMatrix> > ("Actionh");
    }
    catch( std::bad_cast ) 
    {
      QDPIO::cerr << "MGHB" << ": caught dynamic cast error" 
		  << std::endl;
      QDP_abort(1);
    }
    catch (const std::string& e) 
    {
      QDPIO::cerr << "MGHB" << ": std::map call failed: " << e 
		  << std::endl;
      QDP_abort(1);
    }

    multi2d<LatticeColorMatrix>& a = 
      TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> > ("Actiona");
    multi2d<LatticeColorMatrix>& b = 
      TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> > ("Actionb");
    multi2d<LatticeColorMatrix>& c = 
      TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> > ("Actionc");
    multi2d<LatticeColorMatrix>& d = 
      TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> > ("Actiond");
    multi1d<LatticeColorMatrix>& h = 
      TheNamedObjMap::Instance().getData< multi1d<LatticeColorMatrix> > ("Actionh");

    if(l==0){
      // It doesn't like resizing,
      a.resize(Nd,Nd); a=1;
      b.resize(Nd,Nd); b=1;
      c.resize(Nd,Nd); c=1;
      d.resize(Nd,Nd); d=1; 
      h.resize(Nd); h=1;
    }
    else if(l==1){
      for(int mu=0;mu<Nd; mu++)
      {
        for(int nu=0;nu<Nd; nu++)
        {
          if(nu!=mu)
          {
            a[mu][nu][color[0]] =delta[mu]*shift(u[nu],FORWARD,mu)*shift(adj(u[mu]),FORWARD,nu)*adj(delta[nu]);//CHECKED for correct terms

            b[mu][nu][color[0]] = shifter(delta[nu],FORWARD,mu,2)*shift(shift(adj(u[mu]),FORWARD,mu),FORWARD,nu)*shift(adj(u[nu]),FORWARD,mu)*adj(delta[mu]);// CHECKED for correct terms

            c[mu][nu][color[0]] = delta[mu]*shift(shift(adj(u[nu]),FORWARD,mu),BACKWARD,nu)*shift(adj(u[mu]),BACKWARD,nu)*shifter(adj(delta[nu]),BACKWARD,nu,2);// CHECKED for correct terms

            d[mu][nu][color[0]] = shifter(shifter(delta[nu],FORWARD,mu,2),BACKWARD,nu,2)*shift(shift(adj(u[mu]),FORWARD,mu),BACKWARD,nu)*shift(shift(u[nu],FORWARD,mu),BACKWARD,nu)*adj(delta[mu]);// CHECKED for correct terms
          }
        }// end nu

        h[mu][color[0]] = 0;// CHECKED for correct terms
      }// end mu
    
    }
    else if(l>1){    
      for(int mu=0;mu<Nd; mu++)
      {
        LatticeColorMatrix temp,hsum;
        hsum=1; temp=1;
        hsum[color[0]]-=1;
        temp[color[0]]-=1;

        for(int nu=0;nu<Nd; nu++){
          if(nu != mu){	  
            hsum[color[0]] += delta[mu]*(shifter(u[nu],FORWARD,mu,1<<(l-1))*b[mu][nu] + shifter(shifter(adj(u[nu]),FORWARD,mu,1<<(l-1)),BACKWARD,nu,1<<(l-1))*d[mu][nu]) +
	    (shifter(a[mu][nu],FORWARD,mu,1<<(l-1))*shifter(adj(u[nu]),FORWARD,mu,1<<(l-1))+shifter(c[mu][nu],FORWARD,mu,1<<(l-1))*shifter(shifter(u[nu],FORWARD,mu,1<<(l-1)),BACKWARD,nu,1<<(l-1)))*adj(delta[mu]);//checked for correct terms

	    temp = a[mu][nu];
            a[mu][nu][color[0]] =delta[mu]*temp*adj(delta[nu]);//CHECKED for correct terms

            temp = shifter(b[mu][nu],FORWARD,mu,1<<(l-1));
            b[mu][nu][color[0]] = shifter(delta[nu],FORWARD,mu,1<<(l))*temp*adj(delta[mu]);// CHECKED for correct terms

            temp = c[mu][nu];
            c[mu][nu][color[0]] = delta[mu]*temp*shifter(adj(delta[nu]),BACKWARD,nu,1<<(l));// CHECKED for correct terms

            temp = shifter(d[mu][nu],FORWARD,mu,1<<(l-1));
            d[mu][nu][color[0]] = shifter(shifter(delta[nu],FORWARD,mu,1<<(l)),BACKWARD,nu,1<<(l))*temp*adj(delta[mu]);// CHECKED for correct terms
          }
        }
        temp = h[mu];
        h[mu][color[0]] = hsum + delta[mu]*temp+shifter(temp,FORWARD,mu,1<<(l-1))*adj(delta[mu]);//checked for correct terms
      }
    }
  }


//Finds the action on the coarse lattice
Double coarseact(multi1d<LatticeColorMatrix>& u, Set color)
{
  START_CODE();
    int t=(color.numSubsets()-1)/4+1;// t starts as 2^l
    int l=0;
    while(t >= 2){t=t >> 1; l++;}
    multi2d<LatticeColorMatrix> a;
    multi2d<LatticeColorMatrix> b;
    multi2d<LatticeColorMatrix> c;
    multi2d<LatticeColorMatrix> d;
    multi1d<LatticeColorMatrix> h;

      try
      {
        //TheNamedObjMap::Instance().getData< ActionParams >(ameid);
        TheNamedObjMap::Instance().getData< Int >("LEVEL");
        TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> >("Actiona");
        TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> >("Actionb");
        TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> >("Actionc");
        TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix >>("Actiond");
        TheNamedObjMap::Instance().getData< multi1d<LatticeColorMatrix> >("Actionh");
      }
      catch( std::bad_cast ) 
      {
        QDPIO::cerr << ": caught dynamic cast error" 
  		  << std::endl;
        QDP_abort(1);
      }
      catch (const std::string& e) 
      {
        QDPIO::cerr << ": std::map call failed: " << e 
 		  << std::endl;
        QDP_abort(1);
      }
      catch(...)
      {
        // This might happen on any node, so report it
        std::cerr << "MGGauge: caught generic exception making action" << std::endl;
        QDP_abort(1);
      }

      a=TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> >("Actiona");
      b=TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> >("Actionb");
      c=TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> >("Actionc");
      d=TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> >("Actiond");
      h=TheNamedObjMap::Instance().getData< multi1d<LatticeColorMatrix> >("Actionh");


    Double S_pg = zero;
    if(l==0){
      // This is the original plaq_gaugeact code
      for(int mu=1; mu < Nd; mu++)
      {
        for(int nu=0; nu < mu; nu++)
        {
	  Double tmp = 
		sum(real(trace(u[mu]*shift(u[nu],FORWARD,mu)*adj(shift(u[mu],FORWARD,nu))*adj(u[nu]))));

          S_pg += tmp ;
        }
      }
    }

    else if(l>0){
      for(int mu=0; mu<Nd; mu++)
      {
        LatticeColorMatrix temp=0;
        Double tmp=zero;

        // This shouldn't double count the corners and still manage to find h
        for(int nu=0; nu < mu; nu++)
        {
          temp[color[0]]=( a[mu][nu]*adj(u[nu]) + 
		shifter(u[nu],FORWARD,mu,1<<l)* b[mu][nu] + 
		 c[mu][nu]*shifter(u[nu],BACKWARD,nu,1<<l) + 
		shifter(shifter(adj(u[nu]),BACKWARD,nu,1<<l),FORWARD,mu,1<<l)* d[mu][nu])
		*u[mu];
          tmp=sum(real(trace(temp)));
          S_pg += tmp;
        }

        temp[color[0]] =  h[mu]*u[mu];// This term will be 0 for l=1 case
        tmp=sum(real(trace(temp)));
        S_pg += tmp ;
      }
    }

    // Normalize
    S_pg *= Double(-1.0)/Double(Nc);
    
    END_CODE();

    return S_pg;
}

// Finds the action from the fine plaquettes along the lth level coarse links
Double fineact(multi1d<LatticeColorMatrix>& u,int l){
  START_CODE();
  
  Double S_pg = zero;
  multi1d<LatticeColorMatrix> tempu(Nd);

  Set color;
  color.make(ColorFunc(l));
  // Compute the plaquettes
  for(int m=0; m < Nd; ++m){

    // This finds the plaquettes about color=0 sites
    for(int n=m+1; n < Nd; ++n){
      LatticeColorMatrix tempL;
      tempL=0;
      tempL[color[0]]=
		u[m]*shift(u[n],FORWARD,m)*shift(adj(u[m]),FORWARD,n)*adj(u[n])+ 
		u[m]*shift(shift(adj(u[n]),BACKWARD,n),FORWARD,m)*shift(adj(u[m]),BACKWARD,n)*shift(u[n],BACKWARD,n)+
		shift(u[m],BACKWARD,m)*u[n]*shift(shift(adj(u[m]),BACKWARD,m),FORWARD,n)*shift(adj(u[n]),BACKWARD,m)+
		shift(u[m],BACKWARD,m)*shift(adj(u[n]),BACKWARD,n)*shift(shift(adj(u[m]),BACKWARD,m),BACKWARD,n)*shift(shift(u[n],BACKWARD,m),BACKWARD,n);// The 4 plaquettes in the n,m plane about the color=0 site
      Double temp=sum(real(trace(tempL)));

      S_pg+=temp;
    }

    tempu=u;
    // This finds the action between all corner plaquettes
    for(int i=0; i < (1<<l)-2; ++i){
      //shifting takes a some of loops
      multi1d<LatticeColorMatrix> shiftee(Nd);
      for(int p=0; p < Nd; ++p) shiftee[p]=shift(tempu[p],FORWARD,m);
      tempu=shiftee;
      for(int n=0; n < Nd; ++n){
        if(n!=m){
          LatticeColorMatrix tempL;
          tempL=0;
          tempL[color[0]]=tempu[m]*shift(tempu[n],FORWARD,m)*shift(adj(tempu[m]),FORWARD,n)*adj(tempu[n])+tempu[m]*shift(shift(adj(tempu[n]),BACKWARD,n),FORWARD,m)*shift(adj(tempu[m]),BACKWARD,n)*shift(tempu[n],BACKWARD,n);
          Double temp=sum(real(trace(tempL)));
          S_pg+=temp;
        }
      }
    }
  }


  // Normalize
  S_pg *= Double(-1.0)/Double(Nc);

  return S_pg;
}

  void staple(LatticeColorMatrix& u_mu_staple,
		       multi1d<LatticeColorMatrix>& u,
		       int mu, int cb, Set color)
  {
    START_CODE();

    int t=(color.numSubsets()-1)/4+1;// t starts as 2^l
    int l=0;
    while(t >= 2){t=t >> 1; l++;}
    multi2d<LatticeColorMatrix> a;
    multi2d<LatticeColorMatrix> b;
    multi2d<LatticeColorMatrix> c;
    multi2d<LatticeColorMatrix> d;
    multi1d<LatticeColorMatrix> h;

      try
      {
        //TheNamedObjMap::Instance().getData< ActionParams >(nameid);
        TheNamedObjMap::Instance().getData< Int >("LEVEL");
        TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> >("Actiona");
        TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> >("Actionb");
        TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> >("Actionc");
        TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix >>("Actiond");
        TheNamedObjMap::Instance().getData< multi1d<LatticeColorMatrix> >("Actionh");
      }
      catch( std::bad_cast ) 
      {
        QDPIO::cerr << ": caught dynamic cast error" 
  		  << std::endl;
        QDP_abort(1);
      }
      catch (const std::string& e) 
      {
        QDPIO::cerr << ": std::map call failed: " << e 
 		  << std::endl;
        QDP_abort(1);
      }
      catch(...)
      {
        // This might happen on any node, so report it
        std::cerr << "MGGauge: caught generic exception making action" << std::endl;
        QDP_abort(1);
      }

      a=TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> >("Actiona");
      b=TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> >("Actionb");
      c=TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> >("Actionc");
      d=TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> >("Actiond");
      h=TheNamedObjMap::Instance().getData< multi1d<LatticeColorMatrix> >("Actionh");
      color.make(ColorFunc(l));
    
    u_mu_staple = 0;
    LatticeColorMatrix tmp1 = 0;
    LatticeColorMatrix tmp2 = 0;
    if(l==0){
      // This is the original plaq_gaugeact code
      for(int nu=0; nu < Nd; nu++) 
      {

        LatticeColorMatrix u_nu_mu;
        if( nu == mu ) continue;
      
        u_nu_mu = shift(u[nu],FORWARD,mu);

        // +forward staple
        tmp1[rb[cb]] = u_nu_mu * adj(shift(u[mu],FORWARD,nu));
        tmp2[rb[cb]] = tmp1 * adj(u[nu]);

        u_mu_staple[rb[cb]] += tmp2;

        // +backward staple
        tmp1[rb[cb]] = adj(shift(u_nu_mu,BACKWARD,nu)) * adj(shift(u[mu],BACKWARD,nu));
        tmp2[rb[cb]] = tmp1 * shift(u[nu],BACKWARD,nu);

        u_mu_staple[rb[cb]] += tmp2 ;
      }
    }
    else if(l>0){
      for(int nu=0; nu < Nd; nu++) 
      {
        // The original code above never had 2 u in the same line so I changed this.
        if( nu == mu ) continue;
        tmp1[color[0]] = a[mu][nu]*adj(u[nu]);
        tmp1[color[0]] += shifter(u[nu],FORWARD,mu,1<<l)* b[mu][nu] ;
        tmp1[color[0]] += c[mu][nu]*shifter(u[nu],BACKWARD,nu,1<<l);
        tmp1[color[0]] += shifter(shifter(adj(u[nu]),BACKWARD,nu,1<<l),FORWARD,mu,1<<l)* d[mu][nu];
        u_mu_staple[color[0]] += tmp1;
      }
      u_mu_staple[color[0]]+=(h[mu]) ;
    }

    // NOTE: a heatbath code should be responsible for resetting links on
    // a boundary. The staple is not really the correct place.

    END_CODE();
  }

Double stapleTest(multi1d<LatticeColorMatrix> u, Set color)
{

  START_CODE();
  int t=(color.numSubsets()-1)/4+1;// t starts as 2^l
  int l=0;
  while(t >= 2){t=t>>1; l++;}

  Double res=0;
  LatticeComplex st=0;
  LatticeColorMatrix stple=0;
  for(int mu=0;mu<Nd;mu++)
  {
    staple(stple,u,mu, 0,color);
    st[color[0]]=trace(stple*u[mu]);
    res+=sum(real(st));

    // Summing over all staples will double count corners of coarse links, this loop counteracts that.
    for(int n=mu+1; n < Nd; ++n){
      LatticeColorMatrix tempL;
      tempL=0;
      tempL[color[0]]=
		u[mu]*shift(u[n],FORWARD,mu)*shift(adj(u[mu]),FORWARD,n)*adj(u[n])+ 
		u[mu]*shift(shift(adj(u[n]),BACKWARD,n),FORWARD,mu)*shift(adj(u[mu]),BACKWARD,n)*shift(u[n],BACKWARD,n)+
		shift(u[mu],BACKWARD,mu)*u[n]*shift(shift(adj(u[mu]),BACKWARD,mu),FORWARD,n)*shift(adj(u[n]),BACKWARD,mu)+
		shift(u[mu],BACKWARD,mu)*shift(adj(u[n]),BACKWARD,n)*shift(shift(adj(u[mu]),BACKWARD,mu),BACKWARD,n)*shift(shift(u[n],BACKWARD,mu),BACKWARD,n);// The 4 plaquettes in the n,m plane about the color=0 site
      Double temp=sum(real(trace(tempL)));

      res-=temp;
    }
  }
    res *= Double(-1.0)/Double(Nc);

  return res;
}


int main(int argc, char *argv[])
{
  // Put the machine into a known state
  Chroma::initialize(&argc, &argv);

  // Setup the lattice size
  // NOTE: in general, the user will need/want to set the
  // lattice size at run-time
  multi1d<int> nrow(Nd);
  for(int i=0; i < Nd; ++i)
    nrow[i] = 4;         // Set the lattice size to 2^4


  // Insert the lattice size into the Layout
  // There can be additional calls here like setting the number of
  // processors to use in an SMP implementation
  Layout::setLattSize(nrow);
  // Create the lattice layout
  // Afterwards, QDP is useable
  Layout::create();
  // Do some wonderful and amazing things - impress your friends

  // Need to define the global variables
  // What should lattice/previous values look like to test this.
  multi1d<LatticeColorMatrix> u(Nd);
  HotSt(u);
  //HotSt(u);

  multi1d<LatticeColorMatrix> originalu(Nd);
  originalu=u;
    Set color;
    multi1d<LatticeColorMatrix> delta(Nd);
    color.make(ColorFunc(0));

// Initialize the action and create a named object
    try
    {
      TheNamedObjMap::Instance().create<Int>("LEVEL");
      TheNamedObjMap::Instance().create<multi2d<LatticeColorMatrix>>("Actiona");
      TheNamedObjMap::Instance().create<multi2d<LatticeColorMatrix>>("Actionb");
      TheNamedObjMap::Instance().create<multi2d<LatticeColorMatrix>>("Actionc");
      TheNamedObjMap::Instance().create<multi2d<LatticeColorMatrix>>("Actiond");
      TheNamedObjMap::Instance().create<multi1d<LatticeColorMatrix>>("Actionh");
    }
    catch (std::bad_cast)
    {
      QDPIO::cerr <<"doMG" <<": caught dynamic cast error" 
		  << std::endl;
      QDP_abort(1);
    }
    catch (const std::string& e) 
    {
      QDPIO::cerr << "doMG" << ": error creating action: " << e << std::endl;
      QDP_abort(1);
    }

    // Sets first int, but is also done in multigrid.
    TheNamedObjMap::Instance().getData<Int>("LEVEL")=Int(0);


  actcoeff(u,delta,color);// initialize the named objects

  Double fine1=zero;
  fine1=fineact(u,1);

  Double fine2=zero;
  fine2=fineact(u,2);

  Double fine3=zero;
  fine3=fineact(u,3);

  XMLFileWriter xml_out("actcoeff.out.xml");
  push(xml_out,"test");
  push(xml_out,"fine");
  write(xml_out,"fine1",fine1);
  write(xml_out,"fine2",fine2);
  write(xml_out,"fine3",fine3);
  pop(xml_out);
  push(xml_out,"coarse");

  for(int l(1); l <= 3; ++l){
    Double coarse=zero;
    Double stapl=zero;
    multi1d<LatticeColorMatrix> g(Nd);
    multi1d<LatticeColorMatrix> oldu(Nd);
    oldu=u;
    color.make(ColorFunc(l));  
    coarsen(u, delta, g, color);
    actcoeff(oldu, delta, color);

    stapl=stapleTest(u,color);
    QDPIO::cout<<"Coarsened"<<'\n';

    coarse=coarseact(u,color);

    push(xml_out,std::to_string(l));// It didn't like pushing "Level" each time
    write(xml_out,"staple", stapl);
    write(xml_out,"coarse",coarse);
    pop(xml_out);
  }


  pop(xml_out);
  pop(xml_out);
  QDPIO::cout<<"Done"<<'\n';
  // It throws an error at close and I'm not sure why.
  xml_out.close();


  // Need to print out results

  // Possibly shutdown the machine
  Chroma::finalize();
  return 1;
}

