// $Id: t_skeleton.cc,v 1.5 2003-06-07 19:09:32 edwards Exp $
/*! \file
 *  \brief Skeleton of a QDP main program
 */

#include "qdp.h"

using namespace QDP;

// A coloring function to find the active sites on the coarse lattice (when color = 0).
class ColorFunc : public SetFunc{
private:
  int l ;
public:
  ColorFunc(int l_):l(l_)  {}
  
  int operator()(const multi1d<int>& coord) const {
    int a=0;
    for(int mu=0; mu < Nd; ++mu){a+=coord[mu]%(1<<l);}
    return a;
  }
  int numSubsets() const {
    return 4*((1<<l)-1)+1;// for l=1, the possible values are 0, 1, 2, 3, 4
  }  
} ;


int main(int argc, char *argv[])
{
  // Put the machine into a known state
  QDP_initialize(&argc, &argv);

  // Setup the lattice size
  // NOTE: in general, the user will need/want to set the
  // lattice size at run-time
  multi1d<int> nrow(Nd);
  for(int i(0); i < Nd; ++i)
    nrow[i] = 8;         // Set the lattice size to 4^4

  // Insert the lattice size into the Layout
  // There can be additional calls here like setting the number of
  // processors to use in an SMP implementation
  Layout::setLattSize(nrow);

  // Create the lattice layout
  // Afterwards, QDP is useable
  Layout::create();

  // Do some wonderful and amazing things - impress your friends
  multi1d<LatticeColorMatrix> u(Nd);

  XMLFileWriter xml_out("colorLat.out.xml");

  for(int l=2; l<=2;++l){
    Set color;
    push(xml_out, "Level");
    write(xml_out,"l",l) ;
    color.make(ColorFunc(l));
    for(int mu=0;mu<Nd;++mu){
      u[mu]=-1;
      for(int c=0; c<4*((1<<l)-1)+1; ++c){
	u[mu][color[c]]=c ; 
      }
    }
    // Write the lattice to look at in matlab
    write(xml_out,"gauge_field",u[0]);
      
    pop(xml_out);
  }


  // Possibly shutdown the machine
  QDP_finalize();

  exit(0);
}
