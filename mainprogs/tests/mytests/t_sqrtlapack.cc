// $Id: t_skeleton.cc,v 1.5 2003-06-07 19:09:32 edwards Exp $
/*! \file
 *  \brief Skeleton of a QDP main program
 */

#include "chroma.h"
#include <qdp-lapack.h>
#include <iostream>
using namespace Chroma;

Int sign(Real t)
{
  return Int(Real(0) < t) - Int(Real(0) > t);
}

DComplex power(DComplex z, Real n)
{
  Real r(pow(real(z)*real(z)+imag(z)*imag(z),Real(.5)));
  Real phi(acos(real(z)/r));
  Int sgn(sign(asin(imag(z))));
  Real temp=phi;
  phi=temp + (sgn-Int(1))*(temp-Real(3.141592));// In the case of phi=0,pi, this will always map to pi
  return cmplx(pow(r,n)*cos(phi*n),pow(r,n)*sin(phi*n));
}

int main(int argc, char *argv[])
{
  // Put the machine into a known state
  Chroma::initialize(&argc, &argv);
  
  START_CODE();
  // Setup the lattice size
  // NOTE: in general, the user will need/want to set the
  // lattice size at run-time
  multi1d<int> nrow(Nd);
  for(int i=0; i < Nd; ++i)
    nrow[i] = 2;         // Set the lattice size to 2^4

  // Insert the lattice size into the Layout
  // There can be additional calls here like setting the number of
  // processors to use in an SMP implementation
  Layout::setLattSize(nrow);

  // Create the lattice layout
  // Afterwards, QDP is useable
  Layout::create();

  // Do some wonderful and amazing things - impress your friends
  multi1d<LatticeColorMatrix> u(Nd);
  HotSt(u);
  

  multi1d<LatticeColorMatrix> sq(Nd);
  multi1d<int> x(Nd);
  for(int i=0;i<Nd;i++) x[i]=0;
  
  while(x[0] < nrow[0]){
    while(x[1] < nrow[1]){
      while(x[2] < nrow[2]){
        while(x[3] < nrow[3]){ 
          ColorMatrix sqrtu;
  
          multi2d<DComplex> mat;
          mat.resize(Nc,Nc);
 
          ColorMatrix v = peekSite(u[0],x);
  
          for(int i=0; i<Nc; i++)
          {
            for(int j=0; j<Nc; j++)
            {
              mat[i][j]=peekColor(v,i,j);
            }
          }

	  multi1d<DComplex> values;
          values.resize(Nc);
          multi2d<DComplex> vectors;
          vectors.resize(Nc,Nc);

          QDPLapack::zgeev(Nc, mat, values, vectors);

	  ColorMatrix l, s; Complex det(cmplx(Real(1),Real(0)));
          for(int i=0; i<Nc; i++)
          {
            for(int j=0; j<Nc; j++)
            {
              l = pokeColor(l, cmplx(Real(0),Real(0)) ,i,j);
              Complex vec;
              vec = vectors[i][j];
              s = pokeColor(s, vec,i,j);
            }
            Complex eval;
            eval = power(values[i],Real(.5));
            det *= eval;
            l = pokeColor(l, eval, i, i);
          } 
          std::cout<<det<<'\n';

          sqrtu = adj(s)*l*s;
	  sq[0] = pokeSite(sq[0],sqrtu,x);

          x[3]++;
        }
	x[3]=0;
        x[2]++;
      }
      x[2]=0;
      x[1]++;
    }
    x[1]=0;
    x[0]++;
  }  

  XMLFileWriter xml_out("mat.out.xml");
  push(xml_out,"Test");
  write(xml_out,"matrix",u[0]);
  pop(xml_out);
  xml_out.close();

  XMLFileWriter xml_out1("sqrt.out.xml");
  push(xml_out1,"Test");
  write(xml_out1,"sqrt", sq[0]);
  pop(xml_out1);
  xml_out1.close();

  XMLFileWriter xml_out2("diff.out.xml");
  push(xml_out2,"Test");
  write(xml_out2,"sqrt", sq[0]*sq[0]-u[0]);
  pop(xml_out2);
  xml_out2.close();  

  // Possibly shutdown the machine
  END_CODE();

  Chroma::finalize();
  exit(0);
}
