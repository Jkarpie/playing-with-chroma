// $Id: t_skeleton.cc,v 1.5 2003-06-07 19:09:32 edwards Exp $
/*! \file
 *  \brief Skeleton of a QDP main program
 */



#include "chroma.h"
#include <qdp-lapack.h>
#include <ctime>

using namespace Chroma;

// A coloring function to find the active sites on the coarse lattice (when color = 0).
class ColorFunc : public SetFunc{
private:
  int l ;
public:
  ColorFunc(int l_):l(l_)  {}
  
  int operator()(const multi1d<int>& coord) const {
    int a=0;
    for(int mu=0; mu < Nd; mu++){a+=coord[mu]%(1<<l);}
    return a;
  }
  int numSubsets() const {
    return 4*((1<<l)-1)+1;// for l=1, the possible values are 0, 1, 2, 3, 4
  }  
} ;


Int sign(Real t)
{
    Int ans = Int(Real(0) < t) - Int(Real(0) > t);

    // In the case of phi=0/pi, ans= 0 not +1/-1, this will always map to phi to pi,
    // This if statement sets phi to 0 instead.
    if(toBool(ans!=Int(0))) return ans;
    else return Int(1);
}

Complex power(Complex z, Real n)
{
  START_CODE();
  Real r(pow(real(z)*real(z)+imag(z)*imag(z),Real(.5)));
  Real phi(acos(real(z)/r));
  Int sgn(sign(imag(z)));
  Real temp=phi;
  phi=sgn*temp;
  END_CODE();

  // In the case of phi=0,pi, this will always map to pi
  return cmplx(sgn*pow(r,n)*cos(phi*n),sgn*pow(r,n)*sin(phi*n));

}


// Returns LCM a shifted by dist steps
LatticeColorMatrix shifter(const LatticeColorMatrix& a, int sign, int dir, int dist){
  START_CODE();

  LatticeColorMatrix b;
  LatticeColorMatrix c;
  b=a;
  for(int i=0;i<dist;i++){
    c=shift(b,sign,dir);
    b=c;
  }
  END_CODE();
  return b;
}

LatticeColorMatrix sqrt(LatticeColorMatrix& u, Set color)
{
  START_CODE();
  int t=(color.numSubsets()-1)/4+1;// t starts as 2^l
  int l=0;
  while(t >= 2){t=t>>1; l++;}

  LatticeColorMatrix sq;
  multi1d<int> x(Nd);
  for(int i=0;i<Nd;i++) x[i]=0;
  multi1d<int> nrow(Nd);
  nrow = Layout::lattSize();

  LatticeColorVector halfev;
  LatticeColorVector freshev;
  int stepsize=1;
  std::clock_t xtimer;
  while(x[0] < nrow[0]){
    while(x[1] < nrow[1]){
      xtimer=std::clock();
      while(x[2] < nrow[2]){
        while(x[3] < nrow[3]){ 
          ColorMatrix sqrtu;
  
          multi2d<DComplex> mat;
          mat.resize(Nc,Nc);
 
          ColorMatrix v = peekSite(u,x);
          ColorVector halfevtemp;
          ColorVector temp;

	  // Fill the matrix for lapack
          for(int i=0; i<Nc; i++)
          {
            for(int j=0; j<Nc; j++)
            {
              mat[i][j]=peekColor(v,i,j);
            }
          }

	  multi1d<DComplex> values;
          values.resize(Nc);
          multi2d<DComplex> vectors;
          vectors.resize(Nc,Nc);

	  // Find evalues and evectors
          QDPLapack::zgeev(Nc, mat, values, vectors);

	  // Fill CMs with evalue^.5 diagonal and evector columns
	  ColorMatrix l=1;
          ColorMatrix s=1;

	  Complex det(cmplx(Real(1),Real(0)));
          for(int i=0; i<Nc; i++)
          {

            temp = pokeColor(temp,(Complex) values[i], i);

            for(int j=0; j<Nc; j++)
            {
              //l = pokeColor(l, cmplx(Real(0),Real(0)) ,i,j); //if l is set to 1 then all off diag are 0
              Complex vec(vectors[i][j]);
              s = pokeColor(s, vec,i,j);
            }
            Complex eval(power((Complex) values[i],Real(.5)));
	    det *= eval;
            l = pokeColor(l, (Complex) eval, i, i);
            halfevtemp = pokeColor(halfevtemp, (Complex) eval, i);
          }
          sqrtu = adj(s)*l*s;
	  
	  if(toBool(real(det)<Real(0))) sqrtu*=-1;

	  sq = pokeSite(sq,sqrtu,x);	  
	  halfev = pokeSite(halfev,halfevtemp,x);

	  freshev = pokeSite(freshev,temp,x);	  
          x[3] += stepsize;
        }
	x[3] = 0;
        x[2] += stepsize;
      }
      x[2] = 0;


        Double diff = (std::clock() - xtimer) / (double) CLOCKS_PER_SEC;

        QDPIO::cout<<x[0]<<" "<<x[1]<<" "<<x[2]<<" "<<x[3]<<" "<<" took " << diff << " sec"<<std::endl;

      x[1] +=stepsize;
    }
    x[1] = 0;
    x[0] += stepsize;
  }

  XMLFileWriter xml_out21("halfevalues.out.xml");
  push(xml_out21,"LEVEL");
  write(xml_out21,"evalues",halfev);
  pop(xml_out21);
  xml_out21.close();

  XMLFileWriter xml_out22("freshevalues.out.xml");
  push(xml_out22,"LEVEL");
  write(xml_out22,"evalues",freshev);
  pop(xml_out22);
  xml_out22.close();

  // Some of the matrices have det = -1, so reunit for good luck
  reunit(sq);
  END_CODE();
  return sq;
}

  // A method to coarsen u on level l-1 to level l
void coarsen(multi1d<LatticeColorMatrix>& u, multi1d<LatticeColorMatrix>& delta, multi1d<LatticeColorMatrix>& g, Set color)
{
  START_CODE();

  int t=(color.numSubsets()-1)/4+1;// t starts as 2^l
  int l=0;
  while(t >= 2){t=t>>1; l++;}

  for(int mu(0); mu<Nd; mu++)
  {  // Shifts links such that u[color[0]] and u2[color[0]] are the two to be combined.
    LatticeColorMatrix u2;
    LatticeColorMatrix temp;

    u2=1;// May be unnecessary
    u2[color[0]]=shifter(u[mu],FORWARD,mu,(1<<(l-1)));

 
    // Set the gauge transformation by shifting the temp
    g[mu]=1;
    temp=g[mu];
    g[mu]=shift(temp,FORWARD,mu);
    temp= u[mu]*u2;
    g[mu][color[0]]=adj(sqrt(temp,color))*u[mu];
    temp=g[mu];
    g[mu]=shift(temp,BACKWARD,mu);
 
    // Do the gauge transformation
    for(int nu(0); nu < Nd; nu++)
    {
      temp=u[nu];
      u[nu]=g[mu]*temp*adj(shift(g[mu],FORWARD,nu));
    }
    delta[mu][color[0]]=u[mu]*adj(u2);
  }
  END_CODE();
}





void finen(multi1d<LatticeColorMatrix>& u, multi1d<LatticeColorMatrix>& oldu, Set color, multi1d<LatticeColorMatrix>& delta, multi1d<LatticeColorMatrix>& g){
  START_CODE();
  int t=(color.numSubsets()-1)/4+1;// t starts as 2^l
  int l=0;
  while(t >= 2){t=t>>1; l++;}

  // This loop will undo the gauge trasformation that was applied during coarsening, then set u to new values using delta
  for(int m=0; m<Nd; m++){
    LatticeColorMatrix v,u2,temp;

    // Hold this for after gauge transformation is undone.
    v[color[0]] = u[m];

    u2=1;
    u2[color[0]]=shifter(oldu[m],FORWARD,m,(1<<(l-1)));
 
    // Undo the gauge transformation
    for(int nu(0); nu < Nd; nu++)
    {
      temp = u[nu];
      u[nu] = adj(g[m])*temp*shift(g[m],FORWARD,nu);
    }
    
       
    // Apply the fine/coarse link relation
    u[m][color[0]] = v*delta[m];
    temp = shifter(u[m],FORWARD,m,(1<<(l-1)));
    temp[color[0]] = adj(delta[m])*v;
    u[m] = shifter(temp,BACKWARD,m,1<<(l-1));

  }
  END_CODE();
}

int main(int argc, char *argv[])
{
  // Put the machine into a known state
  Chroma::initialize(&argc, &argv);

  // Setup the lattice size
  // NOTE: in general, the user will need/want to set the
  // lattice size at run-time
  multi1d<int> nrow(Nd);
  for(int i=0; i < Nd; i++)
    nrow[i] = 16;         // Set the lattice size to 4^4
nrow[0] = 8;
nrow[1] = 8;
  // Insert the lattice size into the Layout
  // There can be additional calls here like setting the number of
  // processors to use in an SMP implementation
  Layout::setLattSize(nrow);

  // Create the lattice layout
  // Afterwards, QDP is useable
  Layout::create();

  // Do some wonderful and amazing things - impress your friends


  multi1d<LatticeColorMatrix> u(Nd);
  HotSt(u); 
//  u=1;

  multi1d<LatticeColorMatrix> oldu(Nd);
  multi1d<LatticeColorMatrix> delta(Nd);
  multi1d<LatticeColorMatrix> g(Nd);

  
  XMLFileWriter xml_out2("precoarseLat.out.xml");
  push(xml_out2,"LEVEL");
  write(xml_out2,"l",0); 
  write(xml_out2,"GaugeField",u[0]);
  pop(xml_out2);
  xml_out2.close();

  oldu=u;

  Set color;
  color.make(ColorFunc(1));

  DComplex z=cmplx(Real(-0.425138),Real(-0.9051285));
  DComplex z1=cmplx(Real(0.9999999),Real(.0000715));
  DComplex z2=cmplx(Real(1.00),Real(.0000715));
  DComplex z3=cmplx(Real(1.00),Real(-.0000715));

  QDPIO::cout<<"Power " <<power(z,Real(.5))<<std::endl;
  QDPIO::cout<<"Power " <<power(z1,Real(.5))<<std::endl;
  QDPIO::cout<<"Power " <<power(z2,Real(.5))<<std::endl;
  QDPIO::cout<<"Power " <<power(z3,Real(.5))<<std::endl;


  std::clock_t timer;
  timer=std::clock();
  LatticeColorMatrix sqrtu=sqrt(u[0],color);
  Double diff= (std::clock()- timer) / (double) CLOCKS_PER_SEC;

  QDPIO::cout<<"Sqrt took " << diff << " sec"<<std::endl;


  XMLFileWriter xml_out8("diffsqrt.out.xml");
  push(xml_out8,"LEVEL");
  write(xml_out8,"l",0); 
  write(xml_out8,"GaugeField",sqrtu*sqrtu-u[0]);
  pop(xml_out8);
  xml_out8.close();
  
  XMLFileWriter xml_out9("squareroot.out.xml");
  push(xml_out9,"LEVEL");
  write(xml_out9,"l",0); 
  write(xml_out9,"GaugeField",sqrtu);
  pop(xml_out9);
  xml_out9.close();



 /* coarsen(u,delta, g,color);

  XMLFileWriter xml_out("postcoarseLat.out.xml");
  push(xml_out,"LEVEL");
  write(xml_out,"l",1); 
  write(xml_out,"GaugeField",u[0]);
  pop(xml_out);
  xml_out.close();


  XMLFileWriter xml_out0("delta.out.xml");
  push(xml_out0,"LEVEL");
  write(xml_out0,"l",0); 
  write(xml_out0,"GaugeField",delta[0]);
  pop(xml_out0);
  xml_out0.close();
  
  finen(u,oldu,color,delta,g);
  
  XMLFileWriter xml_out3("postfineLat.out.xml");
  push(xml_out3,"LEVEL");
  write(xml_out3,"l",0); 
  write(xml_out3,"GaugeField",u[0]);
  pop(xml_out3);
  xml_out3.close();

  XMLFileWriter xml_out1("diff.out.xml");
  push(xml_out1,"LEVEL");
  write(xml_out1,"l",1); 
  write(xml_out1,"GaugeField",u[0]-oldu[0]);
  pop(xml_out1);
  xml_out1.close();*/



  // I want to run the above a few times to check the coarsening for multiple levels

  // Possibly shutdown the machine
  Chroma::finalize();

  exit(0);
}
