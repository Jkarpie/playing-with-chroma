// -*- C++ -*-
/*! \file
 *  \brief Multigrid monomial for Joe's action.
 */

#ifndef __multigrid_gaugeact_monomial_h__
#define __multigrid_gaugeact_monomial_h__

#include "chromabase.h"
#include "update/molecdyn/field_state.h"
#include "update/molecdyn/monomial/abs_monomial.h"
#include "update/molecdyn/monomial/force_monitors.h"
#include "io/xmllog_io.h"
#include "actions/gauge/gaugeacts/mg_gaugeact.h"

namespace Chroma 
{
  /*! @ingroup monomial */
  namespace MultigridGaugeMonomialEnv 
  {
    extern const std::string name;
    bool registerAll();
  }


  // Parameter structure
  /*! @ingroup monomial */
  struct MultigridGaugeMonomialParams 
  {
    // Base Constructor
    MultigridGaugeMonomialParams();

    // Read monomial from some root path
    MultigridGaugeMonomialParams(XMLReader& in, const std::string& path);
    std::string gauge_act;
  };


  //! Wrapper class for  gauge monomials
  /*! @ingroup monomial
   *
   * Monomial is expected to be the same for these fermacts
   */
  class MultigridGaugeMonomial :
    public ExactMonomial< multi1d<LatticeColorMatrix>,
                          multi1d<LatticeColorMatrix> >    
  {
  public: 
    typedef multi1d<LatticeColorMatrix>  P;
    typedef multi1d<LatticeColorMatrix>  Q;

    //! Construct out of a parameter struct. Check against the desired GaugeAct name
    MultigridGaugeMonomial(const MultigridGaugeMonomialParams& param_);

    //! Copy Constructor
    MultigridGaugeMonomial(const MultigridGaugeMonomial& m) : gaugeact((m.gaugeact)) {}

    //! Create a suitable state and compute F
    void dsdq(P& F, const AbsFieldState<P,Q>& s) 
    {
      START_CODE();

      XMLWriter& xml_out = TheXMLLogWriter::Instance();
      push(xml_out, "MultigridGaugeMonomial");

      // Make a gauge connect state
      Handle< GaugeState<P,Q> > g_state(getGaugeAct().createState(s.getQ()));

      getGaugeAct().deriv(F, g_state);

      monitorForces(xml_out, "Forces", F);
      pop(xml_out);

      END_CODE();
    }


    //! Gauge action value
    Double S(const AbsFieldState<P,Q>& s)  
    {
      START_CODE();

      XMLWriter& xml_out = TheXMLLogWriter::Instance();
      push(xml_out, "MultigridGaugeMonomial");

      Handle< GaugeState<P,Q> > g_state(getGaugeAct().createState(s.getQ()));
      Double action = getGaugeAct().S(g_state);

      write(xml_out, "S", action);
      pop(xml_out);

      END_CODE();

      return action;
    }
	
	
    void refreshInternalFields(const AbsFieldState<P,Q>& s); 
    //Note for Joe: we have to be a little naughty here by declaring this in scope, but letting the actual meat happen in .cc.
    /*{
      //No internal fields to refresh => Nop
    }*/

    void setInternalFields(const Monomial<P,Q>& m) 
    {
      // No internal fields to refresh => Nop
    }

    protected:
      const GaugeAction<P,Q>& getGaugeAct(void) const { 
	return *gaugeact;
      }

    private:
      // Hide empty constructor and =
      MultigridGaugeMonomial();
      void operator=(const MultigridGaugeMonomial&);

    private:
      // A handle to Joe's gaugeact
      Handle< MgGaugeAct > gaugeact;
    };


}; //end namespace chroma

#endif
