// -*- C++ -*-
/*! \file
 *  \brief Multigrid monomial for Joe's action.
 */

#include "chromabase.h"
#include "update/molecdyn/monomial/multigrid_gauge_monomial.h"
#include "update/molecdyn/monomial/monomial_factory.h"
#include "actions/gauge/gaugeacts/gaugeacts_aggregate.h"
#include "actions/gauge/gaugeacts/gaugeact_factory.h"

namespace Chroma 
{ 
  namespace MultigridGaugeMonomialEnv 
  {
    namespace
    {
      //! Callback function for the factory
      Monomial< multi1d<LatticeColorMatrix>,
		multi1d<LatticeColorMatrix> >*
      createMonomial(XMLReader& xml, const std::string& path) 
      {
	QDPIO::cout << "Create monomial: " << name << std::endl;

	return new MultigridGaugeMonomial(MultigridGaugeMonomialParams(xml, path));
      }

      //! Local registration flag
      bool registered = false;
    }

    const std::string name("MULTIGRID_GAUGE_MONOMIAL");
    
    //! Register all the factories
    bool registerAll() 
    {
      bool success = true; 
      if (! registered)
      {
	success &= GaugeActsEnv::registerAll();
	success &= TheMonomialFactory::Instance().registerObject(name, createMonomial);
	registered = true;
      }
      return success;
    }
  } //end namespace MultigridGaugeMonomialEnv



  // Read the parameters
  MultigridGaugeMonomialParams::MultigridGaugeMonomialParams(XMLReader& xml_in, const std::string& path)
  {
    // Get the top of the parameter XML tree
    XMLReader paramtop(xml_in, path);
    
    try {
      // Read the inverter Parameters
      XMLReader xml_tmp(paramtop, "./GaugeAction");
      std::ostringstream os;
      xml_tmp.print(os);
      gauge_act = os.str();
   
    }
    catch(const std::string& s) {
      QDPIO::cerr << "Caught Exception while reading parameters: " << s <<std::endl;
      QDP_abort(1);
    }

    QDPIO::cout << "MultigridGaugeMonomialParams: read \n" << gauge_act << std::endl;
  }

  //! Read Parameters
  void read(XMLReader& xml, const std::string& path,
	    MultigridGaugeMonomialParams& params) 
  {
    MultigridGaugeMonomialParams tmp(xml, path);
    params = tmp;
  }

  //! Write Parameters
  void write(XMLWriter& xml, const std::string& path,
	     const MultigridGaugeMonomialParams& params) 
  {
    // Not implemented
    QDPIO::cerr << MultigridGaugeMonomialEnv::name << ": write not implemented" << std::endl;
    QDP_abort(1);
  }


  // Constructor
  MultigridGaugeMonomial::MultigridGaugeMonomial(const MultigridGaugeMonomialParams& param_) 
  {
    std::istringstream is(param_.gauge_act);
    XMLReader gaugeact_reader(is);

    // Get the name of the gauge act
    std::string gaugeact_string;
    try { 
      read(gaugeact_reader, "/GaugeAction/Name", gaugeact_string);
    }
    catch( const std::string& e) { 
      QDPIO::cerr << "Error grepping the gaugeact name: " << e<<  std::endl;
      QDP_abort(1);
    }

    gaugeact = new MgGaugeAct(CreateGaugeStateEnv::reader(gaugeact_reader, "/GaugeAction"),
                MgGaugeActParams(gaugeact_reader, "/GaugeAction"));
   // gaugeact = new RectGaugeAct(CreateGaugeStateEnv::reader(gaugeact_reader, "/GaugeAction"),
     //           RectGaugeActParams(gaugeact_reader, "/GaugeAction"));
    //gaugeact = new MgGaugeAct(gaugeact_reader,"/GaugeAction");
  }

  void MultigridGaugeMonomial::refreshInternalFields(const AbsFieldState<P,Q>& s)
  {
  	//gaugeact->resetAction();
        //Joe can change this later on, to make add more complicated functionality.
  }

} //end namespace Chroma


