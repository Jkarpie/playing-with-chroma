// -*- C++ -*-
/*! \file
 *  \brief Plaquette gauge action
 */

#ifndef __mg_gaugeact_h__
#define __mg_gaugeact_h__

#include "gaugeact.h"
#include "gaugebc.h"
#include "actions/gauge/gaugestates/gauge_createstate_factory.h"
#include "actions/gauge/gaugestates/gauge_createstate_aggregate.h"

namespace Chroma
{

  /*! @ingroup gaugeacts */
  namespace MgGaugeActEnv 
  { 
    extern const std::string name;
    bool registerAll();
  }
  
//  /*! @ingroup gaugeacts */
//  void write(XMLWriter& xml, const std::string& path, const MgGaugeActParams& param);
  

  //! MultiGrid gauge action
  /*! \ingroup gaugeacts
   *
   * The standard Plaquette gauge action
   */



    //! Parameter structure
    /*! @ingroup gaugeacts */
  class  MgGaugeActParams 
  {
  public:
    //! Base Constructor
    MgGaugeActParams() {}
    ~MgGaugeActParams() {}
    
    Real  beta;
    int l;
    std::string nameid;
    //! Read params from some root path
    MgGaugeActParams(XMLReader& xml_in, const std::string& path);
  };

  class ActionParams
  {
 
  public:
    int l;
    //                                                                 _
    multi2d<LatticeColorMatrix> a; // unextended corner shaped like---  |
    multi2d<LatticeColorMatrix> b; // unextended corner shaped like----- _|  _
    multi2d<LatticeColorMatrix> c; // unextended corner shaped like-------- |
    multi2d<LatticeColorMatrix> d; // unextended corner shaped like-- |_
    multi1d<LatticeColorMatrix> h; // coefficient for term linear in V

    void setl(int l_){ l=l_;}
    ActionParams()
    {
      l=0;
      a.resize(Nd,Nd);  b.resize(Nd,Nd); c.resize(Nd,Nd); d.resize(Nd,Nd); h.resize(Nd);
      a=1; b=1; c=1; d=1; h=1;
    } 
  };

  class MgGaugeAct : public LinearGaugeAction
  {

  public:



    // A constructor to initialize without using a string name
    MgGaugeAct(XMLReader& xml, const std::string& path)/*, 
		multi2d<LatticeColorMatrix>& newa,
		multi2d<LatticeColorMatrix>& newb, 
		multi2d<LatticeColorMatrix>& newc, 
		multi2d<LatticeColorMatrix>& newd,
		multi1d<LatticeColorMatrix>& newh, Set col ) */
    {
      MgGaugeAct(CreateGaugeStateEnv::reader(xml,path), MgGaugeActParams(xml,path));/*, newa, newb, newc, newd, newh, col);*/
    }

    //! Read coeffs from a param struct
    MgGaugeAct(Handle< CreateGaugeState<P,Q> > cgs_, 
		const MgGaugeActParams& p);

    //! Return the set on which the gauge action is defined
    /*! Defined on the even-off (red/black) set */
    const Set& getSet() const {return color;}

    //! Compute staple
    void staple(LatticeColorMatrix& result,
		const Handle< GaugeState<P,Q> >& state,
		int mu, int cb) const;

    //! Compute dS/dU
    void deriv(multi1d<LatticeColorMatrix>& result,
	       const Handle< GaugeState<P,Q> >& state) const;

    //! Produce a gauge create state object
    const CreateGaugeState<P,Q>& getCreateState() const {return *cgs;}


    //! Compute the actions
    Double S(const Handle< GaugeState<P,Q> >& state) const;

    //! Shifts LCM
    LatticeColorMatrix shifter(const LatticeColorMatrix& a, 
		int sign, int dir, int dist) const;

    //! Resets the action matrices for this action
    void resetAction();


    //! Destructor is automatic
    ~MgGaugeAct() {}

  protected:
    MgGaugeAct() {}
    void operator=(const MgGaugeAct& a) {}       //! Hide assignment


  private:
    Handle< CreateGaugeState<P,Q> >  cgs;  /*!< Create Gauge State */
    MgGaugeActParams  param;             /*!< The parameters */
    Set color;



    int l;
    multi2d<LatticeColorMatrix> a;
    multi2d<LatticeColorMatrix> b;
    multi2d<LatticeColorMatrix> c;
    multi2d<LatticeColorMatrix> d;
    multi1d<LatticeColorMatrix> h;
    //Map shifter;
  };

};


#endif
