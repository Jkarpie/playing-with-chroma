/*! \file
 *  \brief Plaquette gauge action
 */

#include "chroma.h"
#include "chromabase.h"
#include "actions/gauge/gaugeacts/mg_gaugeact.h"
#include "actions/gauge/gaugeacts/gaugeact_factory.h"
#include "actions/gauge/gaugestates/gauge_createstate_factory.h"
#include "actions/gauge/gaugestates/gauge_createstate_aggregate.h"
#include "meas/glue/mesplq.h"


namespace Chroma
{

  // QUESTION: Would this be better to put into the h file?
  // A coloring function to find the active sites on the coarse lattice (when color = 0).
  class ColorFunc : public SetFunc{
  private:
    int l ;
  public:
    ColorFunc(int l_):l(l_)  {}
  
    int operator()(const multi1d<int>& coord) const {
      int a=0;
      for(int mu=0; mu < Nd; ++mu){a+=coord[mu]%(1<<l);}
      return a;
    }
    int numSubsets() const {
      return 4*((1<<l)-1)+1;// ex: for l=1, the possible values are 0, 1, 2, 3, 4
    }  
  } ;

  namespace MgGaugeActEnv 
  { 
    GaugeAction< multi1d<LatticeColorMatrix>, 
		 multi1d<LatticeColorMatrix> >* createGaugeAct(XMLReader& xml, 
							       const std::string& path) 
    {
      return new MgGaugeAct(CreateGaugeStateEnv::reader(xml, path),MgGaugeActParams(xml, path));
    }

    const std::string name = "MG_GAUGEACT";

    //! Local registration flag
    static bool registered = false;

    //! Register all the factories
    bool registerAll() 
    {
      bool success = true; 
      if (! registered)
      {
	success &= TheGaugeActFactory::Instance().registerObject(name, createGaugeAct);
	registered = true;
      }
      return success;
    }
  }

  // Params given beta, a,b,c,d,h, and l
  MgGaugeActParams::MgGaugeActParams(XMLReader& xml_in, const std::string& path) 
  {
    try
    { 
      XMLReader paramtop(xml_in, path);
      read(paramtop,"beta",beta);
     // read(paramtop,"l",l);
      paramtop.close();
      nameid="action";
    }
    catch( const std::string& e ) { 
      QDPIO::cerr << "Error reading XML: " <<  e << std::endl;
      QDP_abort(1);
    }
  }


 /* void read(XMLReader& xml, const std::string& path, MgGaugeActParams& p) 
  {
    MgGaugeActParams tmp(xml, path);
    p=tmp;
  }*/

    MgGaugeAct::MgGaugeAct(Handle< CreateGaugeState<P,Q> > cgs_, 
		const MgGaugeActParams& p) : cgs(cgs_), param(p)
    {
      resetAction();
      color.make(ColorFunc(l));
    }


// Returns LCM a shifted by dist steps
// The use of this method as written is inefficient for higher l, in most cases dist = 2^l
  LatticeColorMatrix MgGaugeAct::shifter(const LatticeColorMatrix& a, int sign, int dir, int dist) const
  {
    LatticeColorMatrix b;
    LatticeColorMatrix c;
    b=a;
    for(int i=0;i<dist;i++){
      c=shift(b,sign,dir);
      b=c;
    }
    return b;
  }

  //! Compute staple
  /*!
   * \param u_mu_staple   result      ( Write )
   * \param state         gauge field ( Read )
   * \param mu            direction for staple ( Read )
   * \param cb            subset on which to compute ( Read )
   */
  void
  MgGaugeAct::staple(LatticeColorMatrix& u_mu_staple,
		       const Handle< GaugeState<P,Q> >& state,
		       int mu, int cb) const
  {
    START_CODE();

    const multi1d<LatticeColorMatrix>& u = state->getLinks();
    
    u_mu_staple = 0;
    LatticeColorMatrix tmp1 = 0;
    LatticeColorMatrix tmp2 = 0;
    if(l==0){
      // This is the original plaq_gaugeact code
      for(int nu=0; nu < Nd; nu++) 
      {

        LatticeColorMatrix u_nu_mu;
        if( nu == mu ) continue;
      
        u_nu_mu = shift(u[nu],FORWARD,mu);

        // +forward staple
        tmp1[rb[cb]] = u_nu_mu * adj(shift(u[mu],FORWARD,nu));
        tmp2[rb[cb]] = tmp1 * adj(u[nu]);

        u_mu_staple[rb[cb]] += param.beta * tmp2;

        // +backward staple
        tmp1[rb[cb]] = adj(shift(u_nu_mu,BACKWARD,nu)) * adj(shift(u[mu],BACKWARD,nu));
        tmp2[rb[cb]] = tmp1 * shift(u[nu],BACKWARD,nu);

        u_mu_staple[rb[cb]] += tmp2 * Double(param.beta);
      }
    }
    else if(l>0){
      for(int nu=0; nu < Nd; nu++) 
      {
        // The original code above never had 2 u in the same line so I changed this.
        if( nu == mu ) continue;
        tmp1[color[0]] = a[mu][nu]*adj(u[nu]);
        tmp1[color[0]] += shifter(u[nu],FORWARD,mu,1<<l)* b[mu][nu] ;
        tmp1[color[0]] += c[mu][nu]*shifter(u[nu],BACKWARD,nu,1<<l);
        tmp1[color[0]] += shifter(shifter(adj(u[nu]),BACKWARD,nu,1<<l),FORWARD,mu,1<<l)* d[mu][nu];
        u_mu_staple[color[0]] += tmp1 * Double(param.beta);
      }
      u_mu_staple[color[0]]+=(h[mu]) * Double(param.beta);
    }

    // NOTE: a heatbath code should be responsible for resetting links on
    // a boundary. The staple is not really the correct place.

    END_CODE();
  }

  Double
  MgGaugeAct::S(const Handle< GaugeState<P,Q> >& state) const
  {
    START_CODE();

    Double S_pg = zero;

    // Handle< const GaugeState<P,Q> > u_bc(createState(u));
    // Apply boundaries
    const multi1d<LatticeColorMatrix>& u = state->getLinks();

    // Compute the average plaquettes
    if(l==0){
      // This is the original plaq_gaugeact code
      for(int mu=1; mu < Nd; mu++)
      {
        for(int nu=0; nu < mu; nu++)
        {
	  Double tmp = 
		sum(real(trace(u[mu]*shift(u[nu],FORWARD,mu)*adj(shift(u[mu],FORWARD,nu))*adj(u[nu]))));

          S_pg += tmp * Double(param.beta);
        }
      }
    }

    else if(l>0){
      for(int mu=0; mu<Nd; mu++)
      {
        LatticeColorMatrix temp=0;
        Double tmp=zero;

        for(int nu=0; nu < mu; nu++)
        {
          temp[color[0]]=( a[mu][nu]*adj(u[nu]) + 
		shifter(u[nu],FORWARD,mu,1<<l)* b[mu][nu] + 
		 c[mu][nu]*shifter(u[nu],BACKWARD,nu,1<<l) + 
		shifter(shifter(adj(u[nu]),BACKWARD,nu,1<<l),FORWARD,mu,1<<l)* d[mu][nu])
		*u[mu];
          tmp=sum(real(trace(temp)));
          S_pg += tmp;
        }

        temp[color[0]] =  h[mu]*u[mu];// This term will be 0 for l=1 case
        tmp=sum(real(trace(temp)));
        S_pg += tmp * Double(param.beta);
      }
    }

    // Normalize
    S_pg *= Double(-1.0)/Double(Nc);
    
    END_CODE();

    return S_pg;
  } 


  //! Computes the derivative of the fermionic action respect to the link field
  /*!
   *         |  dS
   * ds_u -- | ----   ( Write )
   *         |  dU  
   *
   * \param ds_u       result      ( Write )
   * \param state      gauge field ( Read )
   */
  void
  MgGaugeAct::deriv(multi1d<LatticeColorMatrix>& ds_u,
		      const Handle< GaugeState<P,Q> >& state) const
  {
    START_CODE();

    ds_u.resize(Nd);
    ds_u = zero;
    const multi1d<LatticeColorMatrix>& u = state->getLinks();

    if(l==0){
      // This is the original plaq_gaugeact code
      LatticeColorMatrix tmp_0;
      LatticeColorMatrix tmp_1;
      LatticeColorMatrix tmp_2;

      for(int mu = 0; mu < Nd; mu++)
      {
        for(int nu=mu+1; nu < Nd; nu++) 
        {
	  for(int cb=0; cb < 2; cb++) 
          { 
            tmp_0[rb[cb]] = shift(u[mu], FORWARD, nu)*shift(adj(u[nu]), FORWARD, mu);
            tmp_0[rb[cb]] *= param.beta;   // c[mu][nu] = c[nu][mu]
            tmp_1[rb[cb]] = tmp_0*adj(u[mu]);
            tmp_2[rb[cb]] = u[nu]*tmp_1;
            ds_u[nu][rb[cb]] += tmp_2;
            ds_u[mu][rb[cb]] += adj(tmp_2);
            ds_u[mu][rb[1-cb]] += shift(tmp_1, BACKWARD, nu)*shift(u[nu], BACKWARD, nu);
            tmp_1[rb[cb]] = adj(u[nu])*u[mu];
            ds_u[nu][rb[1-cb]] += shift(adj(tmp_0),BACKWARD,mu)*shift(tmp_1, BACKWARD, mu);
	  }
        }
      
        // It is 1/(4Nc) to account for normalisation relevant to fermions
        // in the taproj, which is a factor of 2 different from the 
        // one used here.
        ds_u[mu] *= Real(-1)/(Real(2*Nc));
      }
    }
    else if(l>0){
      for(int mu=0; mu<Nd; mu++){
        // The below process will double count the corner terms, so the initial plaq_gaugeact code is used to undo this problem
        LatticeColorMatrix s;
        staple(s,state,mu,0);
        ds_u[mu][color[0]]+= u[mu]*s +adj(u[mu]*s);

        // To fix double counting
        for(int nu=mu+1; nu < Nd; nu++) 
        {
          LatticeColorMatrix plaq;
          plaq=u[mu]*shift(u[nu],FORWARD,mu)*adj(shift(u[mu],FORWARD,nu))*adj(u[nu]);
          ds_u[mu][color[0]]-=plaq + shift(plaq,BACKWARD,mu) + shift(plaq,BACKWARD,nu) + shift(shift(plaq,BACKWARD,mu),BACKWARD,nu);
        }
        ds_u[mu][color[0]] *= Real(-1)/(Real(2*Nc));
      }
    }

    // Zero the force on any fixed boundaries
    getGaugeBC().zero(ds_u);

    END_CODE();
  }

  void MgGaugeAct::resetAction()
  {
    try
    {
      TheNamedObjMap::Instance().getData< Int >("LEVEL");
      TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> >("Actiona");
      TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> >("Actionb");
      TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> >("Actionc");
      TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix >>("Actiond");
      TheNamedObjMap::Instance().getData< multi1d<LatticeColorMatrix> >("Actionh");
    }
    catch( std::bad_cast ) 
    {
      QDPIO::cerr << MgGaugeActEnv::name << ": caught dynamic cast error" 
 		  << std::endl;
      QDP_abort(1);
    }
    catch (const std::string& e) 
    {
      QDPIO::cerr << MgGaugeActEnv::name << ": std::map call failed: " << e 
 		  << std::endl;
      QDP_abort(1);
    }
    catch(...)
    {
      // This might happen on any node, so report it
      std::cerr << "MGGauge: caught generic exception making action" << std::endl;
      QDP_abort(1);
    }
    l=toInt(TheNamedObjMap::Instance().getData< Int >("LEVEL"));
    a = TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> >("Actiona");
    b = TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> >("Actionb");
    c = TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> >("Actionc");
    d = TheNamedObjMap::Instance().getData< multi2d<LatticeColorMatrix> >("Actiond");
    h = TheNamedObjMap::Instance().getData< multi1d<LatticeColorMatrix> >("Actionh");
  }
}

